<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Message extends CI_Controller {

  function __construct()
    {
        parent::__construct();
        $this->load->model('api_model/add_user_model');
        $this->load->model('api_model/update_profile_model');   
        $this->load->model('api_model/Message_model');
        $response = array();        

    }

    public function send_notification(){
      $user_token = $this->input->get_request_header('user-token',TRUE);
      $UserId = $this->input->get_request_header('userId', TRUE);      

      $data = json_decode(file_get_contents('php://input'),true);
      $auth_array = array('user_token'=>$user_token,'userId'=>$UserId);
      $isauthorised = $this->update_profile_model->check_authorisation($auth_array);
      if(empty($isauthorised)){     
        $response = ['Status'=>401, 'Message'=>'User is not Authorised.', 'Data'=>$data ] ;
        header('Content-Type: application/json');
        $result = json_encode($response);
        return print_r($result);exit;
      }
      else
      {
      $insert_new_msg = array();

        if($data['group_id'] != "-1") {        
            $insert_new_msg['group_id'] = $data['group_id'];
        }else{   
          //Check for user group based on sender id and receiver id
          $groupid=$this->Message_model->check_group($data['sender_id'],$data['receiver_id']);
          if($groupid == 0)
          {         
            $new_group = $this->Message_model->create_user_group($data['sender_id'],$data['receiver_id']);
            $insert_new_msg['is_first'] = "1";
            $insert_new_msg['group_id'] = $new_group;
          }
          else
          {
            $insert_new_msg['group_id'] = $groupid;
          }
          
        }

        $grp_user = $this->Message_model->get_group_user($insert_new_msg['group_id']);

        /*ADD MESSAGE IN DB */

        $insert_new_msg['message_body'] = $data['message'];
        $insert_new_msg['message_type'] = $data['message_type'];
        $insert_new_msg['message_from'] = $data['sender_id'];
        $insert_new_msg['message_to'] = implode(',',$data['receiver_id']);

        $message_details = $this->Message_model->insert_user_message($insert_new_msg);

        // print_r($grp_user);
        // exit();
        foreach($grp_user as $key => $val){
          
          if($val['users'] != $data['sender_id']){
            
            $user_data['userId'] = $val['users'];
            $user_details= $this->add_user_model->isExist($user_data);
            $registrationIds = $user_details['device_token']; // device token ID
            // print_r($registrationIds);
            // exit();
            #prep the bundle
             $notification_msg = array
              (
                  'body'  => json_encode($message_details)             
              );

              $fields = array
              (
                'to'    => $registrationIds,
                'notification'  => $notification_msg,       
              );
      
      
              $headers = array
                  (
                    'Authorization: key=' . API_ACCESS_KEY,
                    'Content-Type: application/json'
                  );
            #Send Reponse To FireBase Server  
                $ch = curl_init();
                curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                curl_setopt( $ch,CURLOPT_POST, true );
                curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                // curl_setopt($ch, CURLOPT_SSLCERT, '../crt.pem');
                // curl_setopt($ch, CURLOPT_SSLKEY, '../key.pem');
                // curl_setopt($ch, CURLOPT_SSLCERTPASSWD, 'pass');
                // curl_setopt($ch, CURLOPT_SSLKEYPASSWD, 'pass');
                curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                $result = curl_exec($ch);
                curl_close( $ch );
            #Echo Result Of FireBase Server
            //echo $result;
          }
        }
      $response = ['Status'=>200, 'Message'=>'Message sent successfully.', 'Data'=>$message_details];
      header('Content-Type: application/json');
      $response1 = json_encode($response);
      return print_r($response1);exit;
    }
 }
 public function message_list()
 {
   $user_token = $this->input->get_request_header('user-token',TRUE);
     $UserId = $this->input->get_request_header('userId', TRUE);      

      $data = json_decode(file_get_contents('php://input'),true);
      $auth_array = array('user_token'=>$user_token,'userId'=>$UserId);
      $isauthorised = $this->update_profile_model->check_authorisation($auth_array);
      if(empty($isauthorised)){     
        $response = ['Status'=>401, 'Message'=>'User is not Authorised.', 'Data'=>$data ] ;
        header('Content-Type: application/json');
        $result = json_encode($response);
        return print_r($result);exit;
      }
      else
      {
        $resultss = $this->Message_model->get_message_list($UserId);   
        if(!empty($resultss))
        {
          $response = ['Status'=>200, 'Message'=>'Recent users list.', 'Data'=>array_values($resultss)];
        }
        else
        {
          $response = ['Status'=>201, 'Message'=>'No data found.'];
        }
        header('Content-Type: application/json');
      
      $response1 = json_encode($response);
        echo $response1;
        // exit;
      }
  }

  public function get_old_messages(){

      $UserId = $this->input->get_request_header('userId', TRUE); 
      $user_token = $this->input->get_request_header('user-token',TRUE);  
      $data = json_decode(file_get_contents('php://input'),true);

      $auth_array = array('user_token'=>$user_token,'userId'=>$UserId);
      $isauthorised = $this->update_profile_model->check_authorisation($auth_array);
        if(empty($isauthorised)){     
          $response = ['Status'=>401, 'Message'=>'User is not Authorised.', 'Data'=>$data ] ;
          header('Content-Type: application/json');
          $result = json_encode($response);
          return print_r($result);exit;
        }
        else
        {

        $datas['user_id'] = $UserId;
        $datas['group_id'] = $data['group_id'];
        $datas['receiver_id'] = $data['receiver_id'];
        $datas['sender_id'] = $data['sender_id'];       
        $datas['message_id'] = $data['message_id'];

        $results = $this->Message_model->get_old_messages($datas);
        if(!empty($results))
        {
          $response = ['Status'=>200,'Message'=>'chat message listed successfully', 'Data'=>array_values($results)];
        }
        else
        {
          $response = ['Status'=>201,'Message'=>'No Data Found', 'data'=>array()];
        }
       header('Content-Type: application/json');
          $result = json_encode($response);
          return print_r($result);exit;
      }
  }

  public function get_recent_messages(){

      $UserId = $this->input->get_request_header('userId', TRUE); 
      $user_token = $this->input->get_request_header('user-token',TRUE);  
      $data = json_decode(file_get_contents('php://input'),true);

      $auth_array = array('user_token'=>$user_token,'userId'=>$UserId);
      $isauthorised = $this->update_profile_model->check_authorisation($auth_array);
        if(empty($isauthorised)){     
          $response = ['Status'=>401, 'Message'=>'User is not Authorised.', 'Data'=>$data ] ;
          header('Content-Type: application/json');
          $result = json_encode($response);
          return print_r($result);exit;
        }
        else
        {

        $datas['user_id'] = $UserId;
        $datas['group_id'] = $data['group_id'];
        $datas['receiver_id'] = $data['receiver_id'];
        $datas['message_id'] = $data['message_id'];
         $datas['sender_id'] = $data['sender_id'];   

        $results = $this->Message_model->get_recent_messages($datas);
        if(!empty($results))
        {
          $response = ['Status'=>200,'Message'=>'chat message listed successfully', 'Data'=>array_values($results)];
        }
        else
        {
          $response = ['Status'=>201,'Message'=>'No Data Found'];
        }
       header('Content-Type: application/json');
          $result = json_encode($response);
          return print_r($result);exit;
      }
  }

  public function delete_msg()
    {

      $UserId = $this->input->get_request_header('userId', TRUE); 
      $user_token = $this->input->get_request_header('user-token',TRUE);  
      $data = json_decode(file_get_contents('php://input'),true);

      $auth_array = array('user_token'=>$user_token,'userId'=>$UserId);
      $isauthorised = $this->update_profile_model->check_authorisation($auth_array);
        if(empty($isauthorised)){     
          $response = ['Status'=>401, 'Message'=>'User is not Authorised.', 'Data'=>$data ] ;
          header('Content-Type: application/json');
          $result = json_encode($response);
          return print_r($result);exit;
        }
        else
        {

        $datas['user_id'] = $UserId;
          $datas['group_id'] = $data['group_id'];
          $datas['receiver_id'] = $data['receiver_id'];
          $datas['message_id'] = $data['message_id'];

        $results = $this->Message_model->delete_msg($datas);
        if($results == 1)
          {
          $response = ['Status'=>200,'Message'=>'chat message Deleted successfully.'];
          }
          else
          {
          $response = ['Status'=>201,'message'=>'Something went wrong.'];
          }
      header('Content-Type: application/json');
          $result = json_encode($response);
          return print_r($result);exit;
       }

    }
    public function schedule_message()
    {
      $UserId = $this->input->get_request_header('userId', TRUE); 
      $user_token = $this->input->get_request_header('user-token',TRUE);  
      $data = json_decode(file_get_contents('php://input'),true);

      $auth_array = array('user_token'=>$user_token,'userId'=>$UserId);
      $isauthorised = $this->update_profile_model->check_authorisation($auth_array);
      if(empty($isauthorised)){     
        $response = ['Status'=>401, 'Message'=>'User is not Authorised.', 'Data'=>$data ] ;
        header('Content-Type: application/json');
        $result = json_encode($response);
        return print_r($result);exit;
      }
      else
      {
        $insert_new_msg = array();

        if($data['group_id'] != "-1") {        
            $insert_new_msg['group_id'] = $data['group_id'];
        }else{   
          //Check for user group based on sender id and receiver id
          $groupid=$this->Message_model->check_group($data['sender_id'],$data['receiver_id']);
          if($groupid == 0)
          {         
            $new_group = $this->Message_model->create_user_group($data['sender_id'],$data['receiver_id']);
            $insert_new_msg['is_first'] = "1";
            $insert_new_msg['group_id'] = $new_group;
          }
          else
          {
            $insert_new_msg['group_id'] = $groupid;
          }
          
        }      

        /*ADD MESSAGE IN DB */

        $insert_new_msg['message_body'] = $data['message'];
        $insert_new_msg['message_type'] = $data['message_type'];
        $insert_new_msg['message_from'] = $data['sender_id'];
        $insert_new_msg['message_to'] = implode(',',$data['receiver_id']);
        $insert_new_msg['is_scheduled'] = '1';
    $insert_new_msg['Schdeule_datetime'] = $data['Schdeule_datetime'];

        $message_details = $this->Message_model->insert_user_message($insert_new_msg);
        if($message_details)
        {
          $response = ['Status'=>200, 'Message'=>'Message schedule successfully.', 'Data'=>$message_details];
        }
        else
        {
          $response = ['Status'=>201, 'Message'=>'Message schedule Failed.', 'Data'=>$message_details];
        }
    
      header('Content-Type: application/json');
      $response1 = json_encode($response);
      return print_r($response1);exit;
      }
    }
    public function schedular()
    {
      $data = array('msg'=>'testtt');
      $res=$this->db->insert('test',$data);
      if($res)
      {
        echo "Success";
      }
      else
      {
        echo "Fail";
      }
    }

}