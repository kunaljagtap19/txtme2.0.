<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['api/AddUser']["POST"] = 'api/Add_user/AddUser';
$route['api/AppFriends']["POST"] = 'api/App_friends/appFriends';
$route['api/check_user_registered']["POST"] = 'api/Add_user/check_user_registered';
$route['api/Send_notification']["POST"] = 'api/Message/send_notification';
$route['api/recent_user_list']["POST"] = 'api/Message/message_list';
$route['api/old_message']["POST"] = 'api/Message/get_old_messages';
$route['api/recent_message']["POST"] = 'api/Message/get_recent_messages';
$route['api/delete_message']["POST"] = 'api/Message/delete_msg';
$route['api/schedule_message']["POST"] = 'api/Message/schedule_message';
$route['api/schedular']["GET"] = 'api/Message/schedular';
$route['api/get_recover_users_messages']["POST"] = 'api/Message/get_recover_users_messages';
//added by kunal
$route['Update_profile/UpdateUser']["POST"] = 'api/Update_profile/UpdateUser';
