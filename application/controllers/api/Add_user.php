<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Add_user extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->model('api_model/add_user_model');
        $this->load->model('api_model/update_profile_model');         

    }

    public function AddUser(){

    	$data = json_decode(file_get_contents('php://input'),true);      

   		if(!empty($data['userId']) && !empty($data['mobile'])){

   			if(!empty($data['image']) && $data['image'] != 'default'){
   				$imagepath = 'assets/upload/image/';
   				$isUpload = uploadImage($data['image'],$data['userId'],$imagepath);
          $isUpload_thumb = do_resize($isUpload);
          
   				if($isUpload != false){
   					$data['image'] = $isUpload;
   				}
          if($isUpload_thumb != false){
            $data['thumb_image'] = $isUpload_thumb;
          }
   			}else{
   				$data['image'] = 'default';
          $data['thumb_image'] = 'default';
   			}
        /*generate unique token for user*/
         $data['user_token'] = generateUserToken();        
   			$result = $this->add_user_model->addUser($data);
    		header('Content-Type: application/json');
  			$result = json_encode($result);
  			return print_r($result);exit;
   		}else{
   			$response = array();
			$response = ['Status'=>201, 'Message'=>'Please check user data.', 'Data'=>null ] ;
			header('Content-Type: application/json');
			$response = json_encode($response);
			return print_r($response);exit;
   		}
    }

    public function check_user_registered(){
        $data = json_decode(file_get_contents('php://input'),true);
        $dataFind = substr($data['mobile'],-7);
        $sentData = array('find' => $dataFind, 'getData' => $data['mobile']);
        $result = $this->add_user_model->get_regis_user($sentData);
        if(empty($result)){
            $response = ['Status'=>201, 'Message'=>'User Not Registered.', 'Data'=>'' ] ;
        }else{
            $response = ['Status'=>200, 'Message'=>'User Registered.', 'Data'=>$result ] ;
        }
      header('Content-Type: application/json');
      $response = json_encode($response);
      return print_r($response);exit;
    }   
    
}
