<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Message extends CI_Controller {

  function __construct()
    {
        parent::__construct();
        $this->load->model('api_model/add_user_model');
        $this->load->model('api_model/update_profile_model');   
        $this->load->model('api_model/Message_model');
        $response = array();        

    }

    public function send_notification(){
      $user_token = $this->input->get_request_header('user-token',TRUE);
      $UserId = $this->input->get_request_header('userId', TRUE);      
      $cert = base_url().'application/APNS_dev.pem';
      // echo $user_token;
      // exit();
      $data = json_decode(file_get_contents('php://input'),true);
      $auth_array = array('user_token'=>$user_token,'userId'=>$UserId);
      $isauthorised = $this->update_profile_model->check_authorisation($auth_array);

      if(empty($isauthorised)){     
        $response = ['Status'=>401, 'Message'=>'User is not Authorised.', 'Data'=>$data ] ;
        header('Content-Type: application/json');
        $result = json_encode($response);
        return print_r($result);exit;
      }
      else
      {
      	$insert_new_msg = array();

        if($data['group_id'] != "-1") {        
            $insert_new_msg['group_id'] = $data['group_id'];
        }else{   
          //Check for user group based on sender id and receiver id
          $groupid=$this->Message_model->check_group($data['sender_id'],$data['receiver_id']);
          
          if($groupid == 0)
          {         
            $new_group = $this->Message_model->create_user_group($data['sender_id'],$data['receiver_id']);
            $insert_new_msg['is_first'] = "1";
            $insert_new_msg['group_id'] = $new_group;
          }
          else
          {
            $insert_new_msg['group_id'] = $groupid;
          }
          
        }

        $grp_user = $this->Message_model->get_group_user($insert_new_msg['group_id']);
        // print_r($grp_user);exit;

        /*ADD MESSAGE IN DB */
        if($data['message_type'] == "text")
        {
          $insert_new_msg['message_body'] = $data['message'];
        }
        elseif($data['message_type'] == "image")
        {
          if(!empty($data['message']))
          {
          $imagepath = 'assets/upload/image/';
          $isUpload = uploadImage($data['message'],'',$imagepath);
          $isUpload_thumb = do_resize($isUpload);
          if($isUpload != false){
            $insert_new_msg['message_body'] = $isUpload;
          }
          }
        }
        elseif($data['message_type'] == "doc")
        {
          if(!empty($data['message']))
          {
          $imagepath = 'assets/upload/Documents/';
          $isUpload = uploadDocument($data['message'],$imagepath);          
          if($isUpload != false){
            $insert_new_msg['message_body'] = $isUpload;
          }
          }
        }
        elseif($data['message_type'] == "audio")
        {
          if(!empty($data['message']))
          {
          $imagepath = 'assets/upload/Audio/';
          $isUpload = uploadDocument($data['message'],$imagepath);          
          if($isUpload != false){
            $insert_new_msg['message_body'] = $isUpload;
          }
          }
        }
       
        
        $insert_new_msg['message_type'] = $data['message_type'];
        $insert_new_msg['message_from'] = $data['sender_id'];
        // $insert_new_msg['message_to'] = implode(',',$data['receiver_id']);
        $insert_new_msg['message_to'] = $data['receiver_id'];
        $user_data1['userId'] =  $data['sender_id'];
        $sender_details= $this->add_user_model->isExist($user_data1);
        $senderName =$sender_details['name'];
        $message_details = $this->Message_model->insert_user_message($insert_new_msg);
        $message_details['sendername'] = $this->Message_model->getUserName($message_details['message_from']);


        // print_r($grp_user);
        // exit();
        foreach($grp_user as $key => $val){
          
          // if($val['users'] != $data['sender_id']){
          if(!empty($data['sender_id']) && !empty($data['receiver_id'])){
          	// echo "inside if";
          	// print_r($data['sender_id']);
          	// echo "</br>";
          	// print_r($data['receiver_id']);

            // $user_data['userId'] = $val['users'];
            $user_data['userId'] = $data['receiver_id'];
            // echo "<br>userid <br>";
            // print_r($user_data['userId']);
            $user_details= $this->add_user_model->isExist($user_data);
            // echo "<br>user_details<br>";
            // print_r($user_details);
            $registrationIds = $user_details['device_token']; // device token ID
            $os_type = $user_details['os_type'];
            // print_r($registrationIds);
            // echo "<br>os type<br>";
            // print_r($os_type);
            // exit;
            #prep the bundle
             $notification_msg = array
              (
                  'body'  => json_encode($message_details),
                  'purpose'=>'Send message'             
              );

              $fields = array
              (
                'to'    => $registrationIds, 
                'data'  => $notification_msg    
              );
      
      
              $headers = array
                  (
                    'Authorization: key=' . API_ACCESS_KEY,
                    'Content-Type: application/json'
                  );
              if($os_type == 'A')
              {
              	// echo "inside this<br>headers";
              	// print_r($headers);
              	// echo "inside this<br>fields";
              	// print_r($fields);
                android_notification($headers,$fields);
              }
              // else
              // {
              //   ios_notification($registrationIds,$message_details,$senderName);
              // }
     
      $response = ['Status'=>200, 'Message'=>'Message sent successfully.', 'Data'=>$message_details];
      header('Content-Type: application/json');
      $response1 = json_encode($response);
      return print_r($response1);exit;
    }
 }
    }
 }
 public function message_list()
 {
   $user_token = $this->input->get_request_header('user-token',TRUE);
     $UserId = $this->input->get_request_header('userId', TRUE);      

      $data = json_decode(file_get_contents('php://input'),true);
      $auth_array = array('user_token'=>$user_token,'userId'=>$UserId);
      $isauthorised = $this->update_profile_model->check_authorisation($auth_array);
      if(empty($isauthorised)){     
        $response = ['Status'=>401, 'Message'=>'User is not Authorised.', 'Data'=>$data ] ;
        header('Content-Type: application/json');
        $result = json_encode($response);
        return print_r($result);exit;
      }
      else
      {
        $resultss = $this->Message_model->get_message_list($UserId);
        // print_r($resultss);exit;
        if(!empty($resultss))
        {
          $response = ['Status'=>200, 'Message'=>'Recent users list.', 'Data'=>array_values($resultss)];
        }
        else
        {
          $response = ['Status'=>201, 'Message'=>'No data found.'];
        }
        header('Content-Type: application/json');
      
      $response1 = json_encode($response);
        // echo $response1;
        return print_r($response1);exit;
        // exit;
      }
  }

  public function get_old_messages(){

      $UserId = $this->input->get_request_header('userId', TRUE); 
      $user_token = $this->input->get_request_header('user-token',TRUE);  
      $data = json_decode(file_get_contents('php://input'),true);

      $auth_array = array('user_token'=>$user_token,'userId'=>$UserId);
      $isauthorised = $this->update_profile_model->check_authorisation($auth_array);
        if(empty($isauthorised)){     
          $response = ['Status'=>401, 'Message'=>'User is not Authorised.', 'Data'=>$data ] ;
          header('Content-Type: application/json');
          $result = json_encode($response);
          return print_r($result);exit;
        }
        else
        {

        $datas['user_id'] = $UserId;
        $datas['group_id'] = $data['group_id'];
        $datas['receiver_id'] = $data['receiver_id'];
        $datas['sender_id'] = $data['sender_id'];       
        $datas['message_id'] = $data['message_id'];

        $results = $this->Message_model->get_old_messages($datas);
        if(!empty($results))
        {
          $response = ['Status'=>200,'Message'=>'chat message listed successfully', 'Data'=>array_values($results)];
        }
        else
        {
          $response = ['Status'=>201,'Message'=>'No Data Found', 'data'=>array()];
        }
       header('Content-Type: application/json');
          $result = json_encode($response);
          return print_r($result);exit;
      }
  }

  public function get_recent_messages(){

      $UserId = $this->input->get_request_header('userId', TRUE); 
      $user_token = $this->input->get_request_header('user-token',TRUE);  
      $data = json_decode(file_get_contents('php://input'),true);

      $auth_array = array('user_token'=>$user_token,'userId'=>$UserId);
      $isauthorised = $this->update_profile_model->check_authorisation($auth_array);
        if(empty($isauthorised)){     
          $response = ['Status'=>401, 'Message'=>'User is not Authorised.', 'Data'=>$data ] ;
          header('Content-Type: application/json');
          $result = json_encode($response);
          return print_r($result);exit;
        }
        else
        {

        $datas['user_id'] = $UserId;
        $datas['group_id'] = $data['group_id'];
        $datas['receiver_id'] = $data['receiver_id'];
        $datas['message_id'] = $data['message_id'];
         $datas['sender_id'] = $data['sender_id'];   

        $results = $this->Message_model->get_recent_messages($datas);
        if(!empty($results))
        {
          $response = ['Status'=>200,'Message'=>'chat message listed successfully', 'Data'=>array_values($results)];
        }
        else
        {
          $response = ['Status'=>201,'Message'=>'No Data Found'];
        }
       header('Content-Type: application/json');
          $result = json_encode($response);
          return print_r($result);exit;
      }
  }

  public function delete_msg()
    {

      $UserId = $this->input->get_request_header('userId', TRUE); 
      $user_token = $this->input->get_request_header('user-token',TRUE);  
      $data = json_decode(file_get_contents('php://input'),true);

      $auth_array = array('user_token'=>$user_token,'userId'=>$UserId);
      $isauthorised = $this->update_profile_model->check_authorisation($auth_array);
        if(empty($isauthorised)){     
          $response = ['Status'=>401, 'Message'=>'User is not Authorised.', 'Data'=>$data ] ;
          header('Content-Type: application/json');
          $result = json_encode($response);
          return print_r($result);exit;
        }
        else
        {

        $datas['user_id'] = $data['sender_id'];
        $datas['group_id'] = $data['group_id'];
        $datas['receiver_id'] = $data['receiver_id'];
        $datas['message_id'] = $data['message_id'];

        $user_data1['userId'] =  $data['sender_id'];
        $sender_details= $this->add_user_model->isExist($user_data1);
        $senderName =$sender_details['name'];

        $results = $this->Message_model->delete_msg($datas);        
        $message_details = $this->Message_model->get_message_details($data['message_id']);
        $grp_user = $this->Message_model->get_group_user($data['group_id']);
        foreach($grp_user as $key => $val){
          
        if($val['users'] != $data['sender_id']){
                  
          $user_data['userId'] = $val['users'];
          $user_details= $this->add_user_model->isExist($user_data);
          $registrationIds = $user_details['device_token']; // device token ID
          $os_type = $user_details['os_type'];

           $notification_msg = array
            (
                'body'  => json_encode($message_details),
                'purpose'=>'Send message'             
            );

            $fields = array
            (
              'to'    => $registrationIds,
              'data'  => $notification_msg    
            );
    
            $headers = array
                (
                  'Authorization: key=' . API_ACCESS_KEY,
                  'Content-Type: application/json'
                );
            if($os_type == 'A')
            {
              android_notification($headers,$fields);
            }
            else
            {
              ios_notification($registrationIds,$message_details,$senderName);
            }           
           
          }
       }
        if($results == 1)
          {
          $response = ['Status'=>200,'Message'=>'chat message Deleted successfully.'];
          }
          else
          {
          $response = ['Status'=>201,'message'=>'Something went wrong.'];
          }
      header('Content-Type: application/json');
          $result = json_encode($response);
          return print_r($result);exit;
       }

    }
    public function schedule_message()
    {
      $UserId = $this->input->get_request_header('userId', TRUE); 
      $user_token = $this->input->get_request_header('user-token',TRUE);  
      $data = json_decode(file_get_contents('php://input'),true);

      $auth_array = array('user_token'=>$user_token,'userId'=>$UserId);
      $isauthorised = $this->update_profile_model->check_authorisation($auth_array);
      if(empty($isauthorised)){     
        $response = ['Status'=>401, 'Message'=>'User is not Authorised.', 'Data'=>$data ] ;
        header('Content-Type: application/json');
        $result = json_encode($response);
        return print_r($result);exit;
      }
      else
      {
        $insert_new_msg = array();

        if($data['group_id'] != "-1") {        
            $insert_new_msg['group_id'] = $data['group_id'];
        }else{   
          //Check for user group based on sender id and receiver id
          $groupid=$this->Message_model->check_group($data['sender_id'],$data['receiver_id']);
          if($groupid == 0)
          {         
            $new_group = $this->Message_model->create_user_group($data['sender_id'],$data['receiver_id']);
            $insert_new_msg['is_first'] = "1";
            $insert_new_msg['group_id'] = $new_group;
          }
          else
          {
            $insert_new_msg['group_id'] = $groupid;
          }
          
        }      

        /*ADD MESSAGE IN DB */

        if($data['message_type'] == "text")
        {
          $insert_new_msg['message_body'] = $data['message'];
        }
        elseif($data['message_type'] == "image")
        {
          if(!empty($data['message']))
          {
          $imagepath = 'assets/upload/image/';
          $isUpload = uploadImage($data['message'],'',$imagepath);
          $isUpload_thumb = do_resize($isUpload);
          if($isUpload != false){
            $insert_new_msg['message_body'] = $isUpload;
          }
          }
        }
        $insert_new_msg['message_type'] = $data['message_type'];
        $insert_new_msg['message_from'] = $data['sender_id'];
        $insert_new_msg['message_to'] = implode(',',$data['receiver_id']);
        $insert_new_msg['is_scheduled'] = '1';
    $insert_new_msg['Schedule_datetime'] = $data['Schedule_datetime'];

        $message_details = $this->Message_model->insert_user_message($insert_new_msg);
        if($message_details)
        {
          $response = ['Status'=>200, 'Message'=>'Message schedule successfully.', 'Data'=>$message_details];
        }
        else
        {
          $response = ['Status'=>201, 'Message'=>'Message schedule Failed.', 'Data'=>$message_details];
        }
    
      header('Content-Type: application/json');
      $response1 = json_encode($response);
      return print_r($response1);exit;
      }
    }
    public function schedular()
    {

     $ScheduleMessgae = $this->Message_model->getScheduleMessgae();
     foreach ($ScheduleMessgae as $key => $value) {

     	$grp_user = $this->Message_model->get_group_user($value['group_id']);
     	foreach ($grp_user as $val) {
     		$user_data1['userId'] =  $value['message_from'];
       		$sender_details= $this->add_user_model->isExist($user_data1);
        	$senderName =$sender_details['name'];
     		if($val['users'] != $value['message_from']){
            
            $user_data['userId'] = $val['users'];
            $user_details= $this->add_user_model->isExist($user_data);
            // print_r($user_details);
            $registrationIds = $user_details['device_token']; // device token ID
            $os_type = $user_details['os_type'];
            #prep the bundle
           $message_details = $ScheduleMessgae[$key];
          	// print_r($message_details);
          
             $notification_msg = array
              (
                  'body'  => json_encode($message_details)             
              );

              $fields = array
              (
                'to'    => $registrationIds,
                'notification'  => $notification_msg,       
              );      
      
              $headers = array
                  (
                    'Authorization: key=' . API_ACCESS_KEY,
                    'Content-Type: application/json'
                  );

        
              if($os_type == 'A')
              {
                android_notification($headers,$fields);
              }
              else
              {
                ios_notification($registrationIds,$message_details,$senderName);
              }

            $Status = array('is_scheduled' => '0');    
			$this->db->where('id', $message_details['id']);
			$this->db->update('messages', $Status);
     
    		}
     	}
      }
    }


    public function get_recover_users_messages(){

      $UserId = $this->input->get_request_header('userId', TRUE); 
      $user_token = $this->input->get_request_header('user-token',TRUE);  
      $data = json_decode(file_get_contents('php://input'),true);

      $auth_array = array('user_token'=>$user_token,'userId'=>$UserId);
      $isauthorised = $this->update_profile_model->check_authorisation($auth_array);
        if(empty($isauthorised)){     
          $response = ['Status'=>401, 'Message'=>'User is not Authorised.', 'Data'=>$data ] ;
          header('Content-Type: application/json');
          $result = json_encode($response);
          return print_r($result);exit;
        }
        else
        {
          $datas['user_id'] = $UserId;
        // $datas['group_id'] = $data['group_id'];
        // $datas['receiver_id'] = $data['receiver_id'];
        // $datas['sender_id'] = $data['sender_id'];       
        $datas['message_id'] = $data['message_id'];

        $results = $this->Message_model->get_recover_user_messages($datas);
        // print_r($results);
        
        if(!empty($results)){
          $response = ['Status'=>200,'Message'=>'chat message listed successfully', 'Data'=>array_values($results)];
        }else{
          $response = ['Status'=>201,'Message'=>'No Data Found', 'data'=>array()];
        }
       header('Content-Type: application/json');
          $result = json_encode($response);
          return print_r($result);exit;
      }
  }
   
    public function test()
    {
      $data = json_decode(file_get_contents('php://input'),true);      
      $imagepath = 'assets/upload/image/';
      $isUpload = uploadDocument($data['document'],$imagepath);
      echo $isUpload;
    }
}