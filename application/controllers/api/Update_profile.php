<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Update_profile extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->model('api_model/update_profile_model');
        $this->load->model('api_model/Add_user_model');
        
    }

    public function UpdateUser(){

       $user_token = $this->input->get_request_header('user-token',TRUE);
       $UserId = $this->input->get_request_header('userId', TRUE);      

      $data = json_decode(file_get_contents('php://input'),true);

      $auth_array = array('user_token'=>$user_token,'userId'=>$UserId);
      $isauthorised = $this->update_profile_model->check_authorisation($auth_array);      
      if(empty($isauthorised)){     
        $response = ['Status'=>401, 'Message'=>'User is not Authorised.', 'Data'=>$data ] ;
        header('Content-Type: application/json');
        $result = json_encode($response);
        return print_r($result);exit;
      }else{ 
        
        if($data['isImageChange'] == 1)
        {
          if(!empty($data['image'])){

            if($data['image'] != 'default'){

              if($isauthorised['image'] != 'default' ){
                $path = pathinfo($isauthorised['image']);
                $imagepath = 'assets/upload/image/'.$path['basename'];
                 if(file_exists($imagepath)){unlink($imagepath);}   
                 $thumbimagepath = 'assets/upload/thumb_image/'.$path['basename'];
                 if(file_exists($thumbimagepath)){unlink($thumbimagepath);}
              }

              $imagepath = 'assets/upload/image/';
              $isUpload = uploadImage($data['image'],$UserId,$imagepath);
              $isUpload_thumb = do_resize($isUpload);

              if($isUpload != false){
                $data['image'] = $isUpload;
                $data['thumb_image'] = $isUpload_thumb;
              }

            }else{ 
              
              if($isauthorised['image'] != 'default' ){
                 $path = pathinfo($isauthorised['image']);
                 $imagepath = 'assets/upload/image/'.$path['basename'];               
                  if(file_exists($imagepath)){unlink($imagepath);}   
                 $thumbimagepath = 'assets/upload/thumb_image/'.$path['basename'];
                  if(file_exists($thumbimagepath)){unlink($thumbimagepath);}
              }
              $data['image'] = 'default';
              $data['thumb_image'] = 'default';
            }
            
          }
        }
        unset($data['isImageChange']);
        $result = $this->update_profile_model->UpdateUserInfo($data,$UserId);
        header('Content-Type: application/json');
        $result = json_encode($result);
        return print_r($result);exit;
      }
    }

    public function get_phone_code(){
      $result = $this->update_profile_model->get_phone_code();
      $result = json_encode($result);
      return print_r($result);exit;
    }

}
