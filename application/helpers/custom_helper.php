
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('uploadImage'))
{ 
    function uploadImage($imagePhoto,$UserId,$imagepath){
        $uploadPostImagePath = $imagepath;
        if (!is_dir($uploadPostImagePath)) {
            mkdir($uploadPostImagePath, 0777, TRUE);
        }
        $image = base64_decode($imagePhoto);
        $image_name = md5(uniqid(rand(), true)).strtotime("now");
        $filename = $image_name . '.' .'jpg';
        $path = $uploadPostImagePath.$filename;
        if(file_put_contents($path , $image)){
            chmod($path, 0777);
            $results = base_url().$path;
            return $results;
        }else{
            return false;
        }
    }
}

if ( ! function_exists('isAuthorised'))
{ 
    function isAuthorised($token){
        $token_array = array('1524','4569','7852','8457');
        $token = strrev($token);
        if(in_array($token, $token_array)){
            return true;
        }else{
            return false;
        }
    }
}

if ( ! function_exists('uploadDocument'))
{ 
    function uploadDocument($Document,$docpath){
        if (!is_dir($docpath)) {
            mkdir($docpath, 0777, TRUE);
        }

         $Doc = base64_decode($Document);
         $f = finfo_open();
         $ext = finfo_buffer($f, $Doc, FILEINFO_MIME_TYPE);    
         $extension = explode('/', $ext)[1];
        $doc_name = md5(uniqid(rand(), true)).strtotime("now");
        $filename = $doc_name . '.' .$extension;
        $path = $docpath.$filename;
        if(file_put_contents($path , $Doc)){
            chmod($path, 0777);
            $results = base_url().$path;
            return $results;
        }else{
            return false;
        }
    }
}

if(! function_exists('do_resize')){

    function do_resize($filename)
    {
        $CI =& get_instance();
        $CI->load->library('image_lib');
        $path = pathinfo($filename);
        $imagepath = 'assets/upload/thumb_image/';

        $source_path = $_SERVER['DOCUMENT_ROOT'].'/chatapp/assets/upload/image/' . $path['basename'];
        $target_path = $_SERVER['DOCUMENT_ROOT'].'/chatapp/assets/upload/thumb_image/';

        $config['image_library']  = 'gd2';
        $config['source_image']   = $source_path;       
        $config['create_thumb']   = FALSE;
        // $config['thumb_marker']   = '_thumb';
        $config['maintain_ratio'] = TRUE;
        $config['width']          = 150;
        $config['height']         = 150;
        $config['new_image']      = $target_path; 

        $CI->image_lib->initialize($config);

        if (! $CI->image_lib->resize()) { 
            $CI->image_lib->clear();
            return false;
        }else{          
          $results = base_url().$imagepath.$path['basename'];
          $CI->image_lib->clear();
          return $results;
        }
    }
}

if ( ! function_exists('generateUserToken'))
{ 
    function generateUserToken(){
         return bin2hex(random_bytes(30));
    }
}
if ( ! function_exists('user_authorization'))
{ 
function user_authorization($data){

        $CI =& get_instance();
        $CI->table = 'users';
        $CI->db->select('*');
        $CI->db->from($CI->table);
        $CI->db->where('userId',$data['userId']);
        $CI->db->where('user_token',$data['user_token']);
        return $CI->db->get()->row_array();
    }
}

// if ( ! function_exists('ios_notification'))
// { 
// function ios_notification($deviceToken,$message,$senderName){

//         $CI =& get_instance();
//         $passphrase = '';


//         $ctx = stream_context_create();
//         stream_context_set_option($ctx, 'ssl', 'local_cert', '/var/www/html/chatapp/cert.pem');
//         stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

//         // Open a connection to the APNS server
//         $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

//         if (!$fp)
//             exit("Failed to connect: $err $errstr" . PHP_EOL);
//         if($message['message_body'] == "text")
//         {
//             $bdy = $message['message_body'];
//         }
//         elseif($message['message_type'] == "image")
//         {
//             $bdy = "Photo";
//         }
//          $body['aps'] = array(
//             'alert' => array('title'=>$senderName,'body'=>$bdy),
//             'sound' => 'default',
//             'badge' => '1',            
//         );
//         $body['message'] =$message;


//         $payload = json_encode($body);
//         // print_r($payload);
//         // exit();
        
//         // Build the binary notification
//         $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

//         // echo $msg;
//         $resultss = fwrite($fp, $msg, strlen($msg));
//         // echo $resultss;
//         if (!$resultss)
//             echo 'Message not delivered' . PHP_EOL;

//         else
//             // echo 'Message successfully delivered' . PHP_EOL;
          
//         fclose($fp);
        
//     }
// }
if ( ! function_exists('android_notification'))
{ 
    function android_notification($headers,$fields){
        // print_r($headers);
        // print_r($fields);
        // exit;
        $CI =& get_instance();
        //Send Reponse To FireBase Server  
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        
        $result = curl_exec($ch);
        curl_close( $ch);

        //added by kunal 19/11/19
        $fieldsarr = json_decode($fields['data']['body']); 
        $fieldsjson = $fields['data']['body'];
        $senderid = $fieldsarr->message_from;
        $receiverid = $fieldsarr->message_to;
        $groupid = $fieldsarr->group_id;

        $db_data =  $arrayName = array('senderid' => $senderid,'receiverid' => $receiverid,'field_body' => $fieldsjson,'firebase_res' => $result);
        $CI->db->insert('android_firebase_response',$db_data);
        $res = $CI->db->last_query();
        //End added by kunal 19/11/19
        // print_r($result);exit;
    }
}
?>