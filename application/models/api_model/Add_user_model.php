<?php 
class Add_user_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $this->table = 'users';
        $this->group = 'user_group';
        $this->group_mapping = 'user_group_mapping';
        $this->message = 'messages';         
        $response = array();
    }

    public function addUser($data){

    	$isExistUser = $this->isExist($data);
        $friends = array();

        //$unfriends = array();

        if(!empty($data['contacts']) && is_array($data['contacts'])){

            $friends = $this->getFriends($data['contacts'],array($data['mobile']));

        }
        
    	if(!empty($isExistUser)){

            if($isExistUser['image'] != 'default' ){

                $path = pathinfo($isExistUser['image']);
                $imagepath = 'assets/upload/image/'.$path['basename'];             
                if(file_exists($imagepath)){unlink($imagepath);}               
                $thumbimagepath = 'assets/upload/thumb_image/'.$path['basename']; 
                if(file_exists($thumbimagepath)){unlink($thumbimagepath);}
                                
            }

    		$updateUser = $this->updateUserData($data);
    		$userdata = $this->isExist($data);
            $userdata['friends'] = $friends;
            //$userdata['unfriends'] = $unfriends;
            if(empty($userdata['friends'])){
                $response = ['Status'=>204, 'Message'=>'User register successfully.', 'Data'=>$userdata ] ;
            }else{
    		  $response = ['Status'=>200, 'Message'=>'User register successfully.', 'Data'=>$userdata ] ;
            } 
            return $response;
    	}else{
            unset($data['contacts']);
    		$insertUser = $this->insertUser($data);
    		if($insertUser){
                $insertUser['friends'] = $friends;
                //$insertUser['unfriends'] = $unfriends;
                if(empty($insertUser['friends'])){
                    $response = ['Status'=>204, 'Message'=>'User registered successfully.', 'Data'=>$insertUser ] ;
                }else{
                    $response = ['Status'=>200, 'Message'=>'User registered successfully.', 'Data'=>$insertUser ] ;
                }
            	return $response;
    		}else{
    			$response = ['Status'=>201, 'Message'=>'User is not registered.', 'Data'=>'' ] ;
            	return $response;
    		}
    	}
    }

    public function getFriends($mobiles,$selfMobile){
        $this->db->select('name,userId,mobile,image,thumb_image,status,device_token');
        $this->db->from($this->table);
        $this->db->where_in('mobile',$mobiles);
        $this->db->where_not_in('mobile',$selfMobile);
        return $this->db->get()->result_array();
    }

    public function isExist($data){
        // var_dump($data);exit;
    	$this->db->select('*');
    	$this->db->from($this->table);
    	$this->db->where('userId',$data['userId']);
    	return $this->db->get()->row_array();
    }

    public function updateUserData($data){

	$updateData = array(
    'user_token' => $data['user_token'],
	'device_token' => $data['device_token'],
    'name' => $data['name'],
    'image' => $data['image'],
    'thumb_image' => $data['thumb_image'],
    'status' => $data['status'],
    'last_seen_hide' =>  (isset($data['last_seen_hide'])) ? $data['last_seen_hide'] : '',
    'auto_download' => (isset($data['auto_download'])) ? $data['auto_download'] : '',

    'os_type' => (isset($data['os_type'])) ? $data['os_type'] : ''
	);
    	$this->db->where('userId',$data['userId']);
    	$this->db->update($this->table,$updateData);
    }

    public function insertUser($data){
    	$this->db->insert($this->table,$data);
    	if($this->db->insert_id()){
    		$this->db->select('*');
    		$this->db->from($this->table);
    		$this->db->where('id',$this->db->insert_id());
    		return $this->db->get()->row_array();
    	}
    	else{
            return false;
    	}
    }

    public function get_regis_user($mobiles){
         $this->db->select('*');
         $this->db->from($this->table);
         $data = $this->db->get()->result_array();
         foreach ($data as $key => $value) {
            if (substr($value['mobile'],-7) === $mobiles['find']) {
                return $data[$key];
                exit();
            }
         }
         return false;
    }    
}