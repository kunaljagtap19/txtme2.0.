<?php 
class Message_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $this->table = 'users';
        $this->group = 'user_group';
        $this->group_mapping = 'user_group_mapping';
        $this->message = 'messages';
        $this->load->model('api_model/add_user_model');
        $response = array();
    }
    public function check_group($senderid,$receiver_id)
    {
        
        $final_array = array();
        
        if(count(array($receiver_id)) == 1)
        {
            // $this->db->select('group_id');
            // $this->db->from($this->group_mapping);
            // $this->db->where('is_group', '0');
            // $this->db->where_in('users', $receiver_id);
            // $result = $this->db->get()->result_array();

            // $this->db->select('group_id');
            // $this->db->from($this->group_mapping);
            // $this->db->where('is_group', '0');
            // $this->db->where('users', $senderid);
            // $result1 = $this->db->get()->result();
            
            $this->db->select('group_id');
            $this->db->from($this->group_mapping);
            $this->db->where('is_group', '0');
            $this->db->where('users', $senderid);
            $this->db->where('user_to', $receiver_id);
            $result1 = $this->db->get()->result();

            // print_r($result1);exit;

            // foreach($result as $key=>$val){
            //     if(in_array($val,$result1)){
            //         $final_array[] = $val;
            //     }
            // }
            // if(!empty($final_array))
            // {
            //     // echo "<pre>";
            //     // print_r($result);
            //     // echo "<br/>";      
            //     // print_r($result1);
            //     // echo "<br/>";
            //     // print_r($final_array);
            //     // exit();

            //     return $final_array[0]->group_id;
            // }
            if (!empty($result1)) {
                // print_r($result1[0]->group_id);exit;
                return $result1[0]->group_id;
            }
            else
            {
                return 0;
            }   
        }
    }

    public function create_user_group($from,$to){
        $data=array('name' => '');
        $this->db->insert($this->group,$data);
        $new_group_id = $this->db->insert_id();
        $map_data ['group_id'] = $new_group_id;

        // array_push($to,$from);
    
        
       $arrayCount = count(array($to));
        if($arrayCount >= 1){
            foreach(array($to) as $k=>$v){
                $map_data['users'] = $from;
                $map_data['user_to'] = $v;
                // $map_data['users'] = $v;
                $this->db->insert($this->group_mapping,$map_data);
            }
        }
        return $new_group_id;
    }

    public function get_group_user($group){
        $this->db->select('users,user_to');
        // $this->db->select('users');
        $this->db->from($this->group_mapping);
        $this->db->where('group_id',$group);
        $this->db->where('status','1');
        return $this->db->get()->result_array(); 
    }

    public function insert_user_message($data){
  
           $this->db->insert($this->message,$data);
           if($this->db->insert_id()){
            $this->db->select('*');
            $this->db->from($this->message);
            $this->db->where('id',$this->db->insert_id());
            return $this->db->get()->row_array();
        }
        else{
            return false;
        }
    }

    public function get_message_list($user_id)
    {

    $res =array();
    $this->db->select('users.userId as senderid,
    users.name as senderName,
    users.mobile as contact,
    user_group.id as groupid,
    user_group.name as groupName,
    messages.*');
    $this->db->from($this->table);
    $this->db->join($this->group_mapping, $this->table.'.userId =' .$this->group_mapping.'.users');
    $this->db->join($this->group, $this->group_mapping.'.group_id =' .$this->group.'.id');
    $this->db->join($this->message, $this->group_mapping.'.group_id =' .$this->message.'.group_id');
    $this->db->where($this->table.'.userId',$user_id); 
    $this->db->order_by($this->message.'.id', "DESC");     
    $result = $this->db->get()->result_array();
    $tempArr = array_unique(array_column($result, 'group_id'));
    $res=array_intersect_key($result, $tempArr);
    if (empty($result)) {
    $this->db->select('users.userId as senderid,
    users.name as senderName,
    users.mobile as contact,
    user_group.id as groupid,
    user_group.name as groupName,
    messages.*');
    $this->db->from($this->table);
    $this->db->join($this->group_mapping, $this->table.'.userId =' .$this->group_mapping.'.user_to');
    $this->db->join($this->group, $this->group_mapping.'.group_id =' .$this->group.'.id');
    $this->db->join($this->message, $this->group_mapping.'.group_id =' .$this->message.'.group_id');
    $this->db->where($this->table.'.userId',$user_id); 
    $this->db->order_by($this->message.'.id', "DESC");     
    $res = $this->db->get()->result_array();
        
    }

    // $db_query = $this->db->last_query();
//     print_r($result);
// print_r($res);exit;
    foreach($res as $key => $val)
    {
        // print_r($val);
        // print_r($user_id);
        $data = array(); 
        if($user_id == $val['message_from'])
        {  
            // $data=array('userId' => $val['message_to']);
            $data['userId'] = $val['message_to'];
        }
        elseif($user_id == $val['message_to'])
        {
            // $data=array('userId' => $val['message_from']);
            $data['userId'] = $val['message_from'];
        }
        // var_dump($data);exit;
        $userdata =  $this->add_user_model->isExist($data);
        // print_r($userdata);exit;
        $res[$key]['Recentuser_serverid'] 		= $userdata['id'];
        $res[$key]['Recentuser_id'] 		= $userdata['userId'];
        $res[$key]['Recentuser_Name'] 		= $userdata['name'];
        $res[$key]['Recentuser_number'] 	= $userdata['mobile'];
        $res[$key]['Recentuser_last_seen'] 	= $userdata['last_seen_hide'];
        $res[$key]['Recentuser_image'] 		= $userdata['image'];
        $res[$key]['Recentuser_country_code'] = $userdata['country_code'];
        $res[$key]['Recentuser_thumb_image'] = $userdata['thumb_image'];
        $res[$key]['Recentuser_device_token'] = $userdata['device_token'];
        $res[$key]['Recentuser_user_token'] = $userdata['user_token'];
        $res[$key]['Recentuser_auto_download'] = $userdata['auto_download'];
        $res[$key]['Recentuser_os_type'] = $userdata['os_type'];
        $res[$key]['Recentuser_created_date'] = $userdata['created_date'];
        $res[$key]['Recentuser_modified_date'] = $userdata['modified_date'];
        $res[$key]['Recentuser_status'] = $userdata['status'];
        $data1=array('userId' => $val['message_from']);
        $userdata1 =  $this->add_user_model->isExist($data1);
        $res[$key]['lastuser_id'] = $userdata1['userId'];
        $res[$key]['lastuser_Name'] = $userdata1['name'];
        $res[$key]['lastuser_number'] = $userdata1['mobile'];

    }       

        if(!empty($res))
        {
            return $res;
        }
        else
        {
            return 0;
        }

    }

    public function get_old_messages($data){

        $query = "";
        if($data['group_id'] == "-1")
        {

            $groupid=$this->check_group($data['sender_id'],array($data['receiver_id']));
            if($groupid == "0")
            {
                return 0;
            }
            else
            {
                $query = $this->db->query("SELECT * FROM messages WHERE group_id = '".$groupid."' AND (is_deleted = '0' || is_scheduled = '0') order by id DESC");
            }
        }
        else
        {
            $query = $this->db->query("SELECT * FROM messages WHERE group_id = '".$data['group_id']."' AND (is_deleted = '0' || is_scheduled = '0') order by id DESC");
        }
        
        if($query->num_rows() > 0){
            $result = $query->result_array();
           
            $result_data = array();

            for($i = 0 ; $i < count($result) ; $i++ ){
                if($data['message_id'] == "-1")
                {
                    $result_data = $result;
                }
                else
                {
                    if($result[$i]['id'] <= $data['message_id']){
                    $result_data[$i] = $result[$i];
                }
                }
               
            }
            return $result_data;
        }else{

            return 0;
        }
    }
    public function get_recent_messages($data){

    	$query = "";
    	if($data['group_id'] == "-1")
    	{
    		$groupid=$this->check_group($data['sender_id'],array($data['receiver_id']));
    		if($groupid == "0")
    		{
    			return 0;
    		}
    		else
    		{
    			$query = $this->db->query("SELECT * FROM messages WHERE group_id = '".$groupid."' AND is_deleted = '0' is_scheduled = '0' order by id DESC");
    		}
    	}
    	else
    	{
        $query = $this->db->query("SELECT * FROM messages WHERE group_id = '".$data['group_id']."' AND is_deleted = '0' AND is_scheduled = '0' order by id DESC");
    	}

        if($query->num_rows() > 0){

            $result = $query->result_array();
            $result_data = array();

            for($i = 0 ; $i < count($result) ; $i++ ){
            	if($data['message_id'] == "-1")
            	{
            		$result_data = $result;
            	}
            	else
            	{
	                if($result[$i]['id'] >= $data['message_id']){
	                    $result_data[$i] = $result[$i];
	                }
	            }
            }
            return $result_data;
        }else{

            return 0;
        }
    }
    public function get_message_details($id)
    {
            $this->db->select('*');
            $this->db->from($this->message);
            $this->db->where('id',$id);
            return $this->db->get()->row_array();
    }

    public function delete_msg($data){
        $query = $this->db->query("UPDATE messages SET is_deleted = '1' WHERE message_from = '".$data['user_id']."' AND group_id = '".$data['group_id']."' AND id='".$data['message_id']."'");      

        if($query){
            return 1;

        }else{

           return 0;
        }
    }
    public function getScheduleMessgae()
    {
        $currntdatetime = date('Y-m-d H:i:s');
        $query = $this->db->query("SELECT * FROM messages WHERE is_deleted = '0' AND is_scheduled = '1' AND Schedule_datetime = '".$currntdatetime."' order by id DESC");
        return $result = $query->result_array();
     
    }

    public function getUserName($id)
    {
        $this->db->select('name,userId');
        $this->db->where('userId' , $id);
        $name = $this->db->get($this->table)->result_array();
        if (!empty($name) && $name != "") {
        return $name[0]['name'];
        }
        else{
            return "";
        }
    }

    public function get_recover_user_messages($data){
        $user_id = $data['user_id'];

        $query = "";
        if($data['message_id'] == "-1")
        {
            // $groupid=$this->check_group($data['sender_id'],array($data['receiver_id']));
            // if($groupid == "0")
            // {
            //     return 0;
            // }
            // else
            // {
                $query = $this->db->query("SELECT * FROM messages WHERE (message_from ='".$user_id."' || message_to = '".$user_id."') AND (is_deleted = '0' || is_scheduled = '0') order by id DESC limit 5");
                
            // }
        }
        else
        {
            $offset = $data['message_id'];

            $query = $this->db->query("SELECT * FROM messages WHERE (message_from ='".$user_id."' || message_to = '".$user_id."') AND (is_deleted = '0' || is_scheduled = '0') AND id < ".$offset." order by id DESC limit 5");
        }
        
        if($query->num_rows() > 0){
            $result_data = $query->result_array();
           
            // $result_data = array();

            // for($i = 0 ; $i < count($result) ; $i++ ){
            //     // if($data['message_id'] == "-1")
            //     // {
            //     //     $result_data = $result;
            //     // }
            //     // else
            //     // {
            //         if($result[$i]['id'] <= $data['message_id']){
            //             $result_data[$i] = $result[$i];
            //         }
            //     // }
            // }
            return $result_data;
        }else{

            return 0;
        }
    }

}