<?php 
class Update_profile_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $this->table = 'users';
        $this->country_table = 'country';
        $response = array();
    }

    public function check_authorisation($auth_data){
        return $isAuthorizedUser = user_authorization($auth_data);
    	// return $isExistUser = $this->isExist($auth_data);
    } 

    public function isExist($data){
    	$this->db->select('*');
    	$this->db->from($this->table);
    	$this->db->where('userId',$data['userId']);
        $this->db->where('device_token',$data['device_token']);        
    	return $this->db->get()->row_array();
    }

    public function UpdateUserInfo($updateInfo,$UserId){         
    	$this->db->where('userId',$UserId);               
    	$this->db->update($this->table,$updateInfo);
        $userinfo = $this->get_user_info($UserId);
        $response = ['Status'=>200, 'Message'=>'User Information Updated Successfully.', 'Data'=>$userinfo ] ;
            return $response;
    }

    public function get_user_info($UserId){
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('userId',$UserId);      
        return $this->db->get()->row_array();
    }

    public function get_phone_code(){
        $this->db->select('iso,phonecode,name');
        $this->db->from($this->country_table);
        $phoneinfo =  $this->db->get()->result_array();
        return $response = ['Status'=>200, 'Message'=>'Phone codes fetched successfully.', 'Data'=>$phoneinfo ] ;   
        
    }
    
}