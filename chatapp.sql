-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 27, 2019 at 05:57 AM
-- Server version: 10.2.10-MariaDB
-- PHP Version: 7.2.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chatapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `android_firebase_response`
--

CREATE TABLE `android_firebase_response` (
  `id` int(11) NOT NULL,
  `senderid` varchar(60) NOT NULL,
  `receiverid` varchar(60) NOT NULL,
  `field_body` text NOT NULL,
  `firebase_res` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `android_firebase_response`
--

INSERT INTO `android_firebase_response` (`id`, `senderid`, `receiverid`, `field_body`, `firebase_res`, `created_at`) VALUES
(1, 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', '{\"id\":\"1\",\"message_type\":\"text\",\"message_body\":\"hi\",\"message_from\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"message_to\":\"WOwg1gJcN6ZeEOwuV7ZSqNVg4s92\",\"group_id\":\"1\",\"seen\":\"0\",\"is_first\":\"1\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-25 18:35:10\",\"modified_date\":\"2019-11-25 18:35:10\",\"sendername\":\"Tanmay\"}', '{\"multicast_id\":1574291962456731894,\"success\":0,\"failure\":1,\"canonical_ids\":0,\"results\":[{\"error\":\"NotRegistered\"}]}`', '2019-11-26 06:28:39'),
(2, 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', '{\"id\":\"2\",\"message_type\":\"text\",\"message_body\":\"xhdhfhchch\",\"message_from\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"message_to\":\"WOwg1gJcN6ZeEOwuV7ZSqNVg4s92\",\"group_id\":\"1\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-25 18:35:17\",\"modified_date\":\"2019-11-25 18:35:17\",\"sendername\":\"Tanmay\"}', '{\"multicast_id\":3744331678408349313,\"success\":0,\"failure\":1,\"canonical_ids\":0,\"results\":[{\"error\":\"NotRegistered\"}]}', '2019-11-25 13:05:17'),
(3, 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', '{\"id\":\"3\",\"message_type\":\"text\",\"message_body\":\" bcbfhfhf\",\"message_from\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"message_to\":\"WOwg1gJcN6ZeEOwuV7ZSqNVg4s92\",\"group_id\":\"1\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-25 18:35:17\",\"modified_date\":\"2019-11-25 18:35:17\",\"sendername\":\"Tanmay\"}', '{\"multicast_id\":5626837621721780604,\"success\":0,\"failure\":1,\"canonical_ids\":0,\"results\":[{\"error\":\"NotRegistered\"}]}', '2019-11-25 13:05:18'),
(4, 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', '{\"id\":\"4\",\"message_type\":\"text\",\"message_body\":\"tuioyh\",\"message_from\":\"WOwg1gJcN6ZeEOwuV7ZSqNVg4s92\",\"message_to\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"group_id\":\"1\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-25 19:19:29\",\"modified_date\":\"2019-11-25 19:19:29\",\"sendername\":\"kunal\"}', '{\"multicast_id\":7184137645102778557,\"success\":0,\"failure\":1,\"canonical_ids\":0,\"results\":[{\"error\":\"NotRegistered\"}]}', '2019-11-25 13:49:29'),
(5, 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', '{\"id\":\"5\",\"message_type\":\"text\",\"message_body\":\"\\ud83d\\ude01\",\"message_from\":\"WOwg1gJcN6ZeEOwuV7ZSqNVg4s92\",\"message_to\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"group_id\":\"1\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-25 22:51:04\",\"modified_date\":\"2019-11-25 22:51:04\",\"sendername\":\"kunal\"}', '{\"multicast_id\":7943785937916729532,\"success\":0,\"failure\":1,\"canonical_ids\":0,\"results\":[{\"error\":\"NotRegistered\"}]}', '2019-11-25 17:21:05'),
(6, 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', '{\"id\":\"6\",\"message_type\":\"text\",\"message_body\":\"\\ud83e\\udd11\",\"message_from\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"message_to\":\"WOwg1gJcN6ZeEOwuV7ZSqNVg4s92\",\"group_id\":\"1\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 11:33:10\",\"modified_date\":\"2019-11-26 11:33:10\",\"sendername\":\"Tanmay\"}', '{\"multicast_id\":6573303032865411240,\"success\":0,\"failure\":1,\"canonical_ids\":0,\"results\":[{\"error\":\"NotRegistered\"}]}', '2019-11-26 06:03:11'),
(7, 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', '{\"id\":\"7\",\"message_type\":\"text\",\"message_body\":\"notification\",\"message_from\":\"WOwg1gJcN6ZeEOwuV7ZSqNVg4s92\",\"message_to\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"group_id\":\"1\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 12:48:28\",\"modified_date\":\"2019-11-26 12:48:28\",\"sendername\":\"kunal\"}', '{\"multicast_id\":1865273679890163398,\"success\":0,\"failure\":1,\"canonical_ids\":0,\"results\":[{\"error\":\"NotRegistered\"}]}', '2019-11-26 07:18:28'),
(8, 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', '{\"id\":\"15\",\"message_type\":\"text\",\"message_body\":\"postman\",\"message_from\":\"WOwg1gJcN6ZeEOwuV7ZSqNVg4s92\",\"message_to\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"group_id\":\"1\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 13:15:22\",\"modified_date\":\"2019-11-26 13:15:22\",\"sendername\":\"kunal\"}', '{\"multicast_id\":816736275388444026,\"success\":1,\"failure\":0,\"canonical_ids\":0,\"results\":[{\"message_id\":\"0:1574754322425212%651d26c5f9fd7ecd\"}]}', '2019-11-26 07:45:22'),
(9, 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', '{\"id\":\"16\",\"message_type\":\"text\",\"message_body\":\"Tanmay \",\"message_from\":\"WOwg1gJcN6ZeEOwuV7ZSqNVg4s92\",\"message_to\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"group_id\":\"1\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 13:16:17\",\"modified_date\":\"2019-11-26 13:16:17\",\"sendername\":\"kunal\"}', '{\"multicast_id\":4923435954198338094,\"success\":1,\"failure\":0,\"canonical_ids\":0,\"results\":[{\"message_id\":\"0:1574754378047388%651d26c5f9fd7ecd\"}]}', '2019-11-26 07:46:18'),
(10, 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', '{\"id\":\"17\",\"message_type\":\"text\",\"message_body\":\"hello  kunal\",\"message_from\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"message_to\":\"WOwg1gJcN6ZeEOwuV7ZSqNVg4s92\",\"group_id\":\"1\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 13:17:05\",\"modified_date\":\"2019-11-26 13:17:05\",\"sendername\":\"Tanmay\"}', '{\"multicast_id\":6481373087179734954,\"success\":0,\"failure\":1,\"canonical_ids\":0,\"results\":[{\"error\":\"NotRegistered\"}]}', '2019-11-26 07:47:05'),
(11, 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', '{\"id\":\"18\",\"message_type\":\"text\",\"message_body\":\"fufy\",\"message_from\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"message_to\":\"WOwg1gJcN6ZeEOwuV7ZSqNVg4s92\",\"group_id\":\"1\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 13:17:28\",\"modified_date\":\"2019-11-26 13:17:28\",\"sendername\":\"Tanmay\"}', '{\"multicast_id\":1457470103872985968,\"success\":0,\"failure\":1,\"canonical_ids\":0,\"results\":[{\"error\":\"NotRegistered\"}]}', '2019-11-26 07:47:28'),
(12, 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', '{\"id\":\"19\",\"message_type\":\"text\",\"message_body\":\"postman tanmay\",\"message_from\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"message_to\":\"WOwg1gJcN6ZeEOwuV7ZSqNVg4s92\",\"group_id\":\"1\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 13:52:51\",\"modified_date\":\"2019-11-26 13:52:51\",\"sendername\":\"Tanmay\"}', '{\"multicast_id\":5461146700385145822,\"success\":0,\"failure\":1,\"canonical_ids\":0,\"results\":[{\"error\":\"NotRegistered\"}]}', '2019-11-26 08:22:51'),
(13, 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', '{\"id\":\"21\",\"message_type\":\"text\",\"message_body\":\"postman tanmay\",\"message_from\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"message_to\":\"WOwg1gJcN6ZeEOwuV7ZSqNVg4s92\",\"group_id\":\"1\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 13:57:41\",\"modified_date\":\"2019-11-26 13:57:41\",\"sendername\":\"Tanmay\"}', '{\"multicast_id\":5344308327679570644,\"success\":0,\"failure\":1,\"canonical_ids\":0,\"results\":[{\"error\":\"NotRegistered\"}]}', '2019-11-26 08:27:41'),
(14, 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', '{\"id\":\"24\",\"message_type\":\"text\",\"message_body\":\"postman tanmay\",\"message_from\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"message_to\":\"WOwg1gJcN6ZeEOwuV7ZSqNVg4s92\",\"group_id\":\"1\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 14:20:26\",\"modified_date\":\"2019-11-26 14:20:26\",\"sendername\":\"Tanmay\"}', '{\"multicast_id\":673708856353602850,\"success\":0,\"failure\":1,\"canonical_ids\":0,\"results\":[{\"error\":\"NotRegistered\"}]}', '2019-11-26 08:50:26'),
(15, 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', '{\"id\":\"28\",\"message_type\":\"text\",\"message_body\":\"postman tanmay\",\"message_from\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"message_to\":\"WOwg1gJcN6ZeEOwuV7ZSqNVg4s92\",\"group_id\":\"1\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 14:29:27\",\"modified_date\":\"2019-11-26 14:29:27\",\"sendername\":\"Tanmay\"}', '{\"multicast_id\":8739034220856924211,\"success\":0,\"failure\":1,\"canonical_ids\":0,\"results\":[{\"error\":\"NotRegistered\"}]}', '2019-11-26 08:59:27'),
(16, 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', '{\"id\":\"35\",\"message_type\":\"text\",\"message_body\":\"postman tanmay\",\"message_from\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"message_to\":\"WOwg1gJcN6ZeEOwuV7ZSqNVg4s92\",\"group_id\":\"1\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 14:41:46\",\"modified_date\":\"2019-11-26 14:41:46\",\"sendername\":\"Tanmay\"}', '{\"multicast_id\":1847572834685846567,\"success\":0,\"failure\":1,\"canonical_ids\":0,\"results\":[{\"error\":\"NotRegistered\"}]}', '2019-11-26 09:11:46'),
(17, 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', '{\"id\":\"36\",\"message_type\":\"text\",\"message_body\":\"postman tanmay\",\"message_from\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"message_to\":\"WOwg1gJcN6ZeEOwuV7ZSqNVg4s92\",\"group_id\":\"1\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 14:47:22\",\"modified_date\":\"2019-11-26 14:47:22\",\"sendername\":\"Tanmay\"}', '{\"multicast_id\":8279512323827549207,\"success\":0,\"failure\":1,\"canonical_ids\":0,\"results\":[{\"error\":\"NotRegistered\"}]}', '2019-11-26 09:17:22'),
(18, 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'QdF47o6FVmapiSPeLonFxJWxboN2', '{\"id\":\"37\",\"message_type\":\"text\",\"message_body\":\"hi nayan\",\"message_from\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"message_to\":\"QdF47o6FVmapiSPeLonFxJWxboN2\",\"group_id\":\"2\",\"seen\":\"0\",\"is_first\":\"1\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 15:06:13\",\"modified_date\":\"2019-11-26 15:06:13\",\"sendername\":\"Tanmay\"}', '{\"multicast_id\":2876178504318175245,\"success\":0,\"failure\":1,\"canonical_ids\":0,\"results\":[{\"error\":\"NotRegistered\"}]}', '2019-11-26 09:36:13'),
(19, 'QdF47o6FVmapiSPeLonFxJWxboN2', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', '{\"id\":\"38\",\"message_type\":\"text\",\"message_body\":\"Vhgh\",\"message_from\":\"QdF47o6FVmapiSPeLonFxJWxboN2\",\"message_to\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"group_id\":\"2\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 15:07:32\",\"modified_date\":\"2019-11-26 15:07:32\",\"sendername\":\"Nayan\"}', '{\"multicast_id\":1605089347287499398,\"success\":1,\"failure\":0,\"canonical_ids\":0,\"results\":[{\"message_id\":\"0:1574761052660076%651d26c5f9fd7ecd\"}]}', '2019-11-26 09:37:32'),
(20, 'QdF47o6FVmapiSPeLonFxJWxboN2', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', '{\"id\":\"39\",\"message_type\":\"text\",\"message_body\":\"Chvhvugy\",\"message_from\":\"QdF47o6FVmapiSPeLonFxJWxboN2\",\"message_to\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"group_id\":\"2\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 15:08:13\",\"modified_date\":\"2019-11-26 15:08:13\",\"sendername\":\"Nayan\"}', '{\"multicast_id\":13298311221734648,\"success\":1,\"failure\":0,\"canonical_ids\":0,\"results\":[{\"message_id\":\"0:1574761093366707%651d26c5f9fd7ecd\"}]}', '2019-11-26 09:38:13'),
(21, 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'QdF47o6FVmapiSPeLonFxJWxboN2', '{\"id\":\"40\",\"message_type\":\"text\",\"message_body\":\"xgdgd\",\"message_from\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"message_to\":\"QdF47o6FVmapiSPeLonFxJWxboN2\",\"group_id\":\"2\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 15:21:18\",\"modified_date\":\"2019-11-26 15:21:18\",\"sendername\":\"Tanmay\"}', '{\"multicast_id\":9187865889411346860,\"success\":0,\"failure\":1,\"canonical_ids\":0,\"results\":[{\"error\":\"NotRegistered\"}]}', '2019-11-26 09:51:18'),
(22, 'QdF47o6FVmapiSPeLonFxJWxboN2', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', '{\"id\":\"41\",\"message_type\":\"text\",\"message_body\":\"Gufhfhfu\",\"message_from\":\"QdF47o6FVmapiSPeLonFxJWxboN2\",\"message_to\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"group_id\":\"2\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 15:21:33\",\"modified_date\":\"2019-11-26 15:21:33\",\"sendername\":\"Nayan\"}', '{\"multicast_id\":3813528036206129900,\"success\":1,\"failure\":0,\"canonical_ids\":0,\"results\":[{\"message_id\":\"0:1574761893336411%651d26c5f9fd7ecd\"}]}', '2019-11-26 09:51:33'),
(23, 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'QdF47o6FVmapiSPeLonFxJWxboN2', '{\"id\":\"42\",\"message_type\":\"text\",\"message_body\":\"fhfufu\",\"message_from\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"message_to\":\"QdF47o6FVmapiSPeLonFxJWxboN2\",\"group_id\":\"2\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 16:58:07\",\"modified_date\":\"2019-11-26 16:58:07\",\"sendername\":\"Tanmay\"}', '{\"multicast_id\":2974613051896316221,\"success\":1,\"failure\":0,\"canonical_ids\":0,\"results\":[{\"message_id\":\"0:1574767687233308%651d26c5f9fd7ecd\"}]}', '2019-11-26 11:28:07'),
(24, 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', '{\"id\":\"43\",\"message_type\":\"text\",\"message_body\":\"hello test1\",\"message_from\":\"WOwg1gJcN6ZeEOwuV7ZSqNVg4s92\",\"message_to\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"group_id\":\"1\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 17:08:48\",\"modified_date\":\"2019-11-26 17:08:48\",\"sendername\":\"kunal\"}', '{\"multicast_id\":3053906559954704269,\"success\":1,\"failure\":0,\"canonical_ids\":0,\"results\":[{\"message_id\":\"0:1574768328654521%651d26c5f9fd7ecd\"}]}', '2019-11-26 11:38:48'),
(25, 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', '{\"id\":\"44\",\"message_type\":\"text\",\"message_body\":\"ok notification\",\"message_from\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"message_to\":\"WOwg1gJcN6ZeEOwuV7ZSqNVg4s92\",\"group_id\":\"1\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 17:09:54\",\"modified_date\":\"2019-11-26 17:09:54\",\"sendername\":\"Tanmay\"}', '{\"multicast_id\":5041805606078565065,\"success\":1,\"failure\":0,\"canonical_ids\":0,\"results\":[{\"message_id\":\"0:1574768395174678%651d26c5f9fd7ecd\"}]}', '2019-11-26 11:39:55'),
(26, 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 'QdF47o6FVmapiSPeLonFxJWxboN2', '{\"id\":\"45\",\"message_type\":\"text\",\"message_body\":\"hello test1\",\"message_from\":\"WOwg1gJcN6ZeEOwuV7ZSqNVg4s92\",\"message_to\":\"QdF47o6FVmapiSPeLonFxJWxboN2\",\"group_id\":\"3\",\"seen\":\"0\",\"is_first\":\"1\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 17:10:11\",\"modified_date\":\"2019-11-26 17:10:11\",\"sendername\":\"kunal\"}', '{\"multicast_id\":7083137170408854098,\"success\":1,\"failure\":0,\"canonical_ids\":0,\"results\":[{\"message_id\":\"0:1574768412040143%651d26c5f9fd7ecd\"}]}', '2019-11-26 11:40:12'),
(27, 'QdF47o6FVmapiSPeLonFxJWxboN2', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', '{\"id\":\"46\",\"message_type\":\"text\",\"message_body\":\"Vjcgcgcg\",\"message_from\":\"QdF47o6FVmapiSPeLonFxJWxboN2\",\"message_to\":\"WOwg1gJcN6ZeEOwuV7ZSqNVg4s92\",\"group_id\":\"3\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 17:10:41\",\"modified_date\":\"2019-11-26 17:10:41\",\"sendername\":\"Nayan2\"}', '{\"multicast_id\":3754122286361355029,\"success\":1,\"failure\":0,\"canonical_ids\":0,\"results\":[{\"message_id\":\"0:1574768442228397%651d26c5f9fd7ecd\"}]}', '2019-11-26 11:40:42'),
(28, 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', '{\"id\":\"47\",\"message_type\":\"text\",\"message_body\":\"vhchfyfy\",\"message_from\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"message_to\":\"WOwg1gJcN6ZeEOwuV7ZSqNVg4s92\",\"group_id\":\"1\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 17:10:55\",\"modified_date\":\"2019-11-26 17:10:55\",\"sendername\":\"Tanmay\"}', '{\"multicast_id\":4826665663722390648,\"success\":1,\"failure\":0,\"canonical_ids\":0,\"results\":[{\"message_id\":\"0:1574768455228192%651d26c5f9fd7ecd\"}]}', '2019-11-26 11:40:55'),
(29, 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', '{\"id\":\"48\",\"message_type\":\"text\",\"message_body\":\" hchf\",\"message_from\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"message_to\":\"WOwg1gJcN6ZeEOwuV7ZSqNVg4s92\",\"group_id\":\"1\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 17:10:56\",\"modified_date\":\"2019-11-26 17:10:56\",\"sendername\":\"Tanmay\"}', '{\"multicast_id\":1725894877665127872,\"success\":1,\"failure\":0,\"canonical_ids\":0,\"results\":[{\"message_id\":\"0:1574768456586939%651d26c5f9fd7ecd\"}]}', '2019-11-26 11:40:56'),
(30, 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', '{\"id\":\"49\",\"message_type\":\"text\",\"message_body\":\"vhfufyfy\",\"message_from\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"message_to\":\"WOwg1gJcN6ZeEOwuV7ZSqNVg4s92\",\"group_id\":\"1\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 17:10:57\",\"modified_date\":\"2019-11-26 17:10:57\",\"sendername\":\"Tanmay\"}', '{\"multicast_id\":4327772842141426384,\"success\":1,\"failure\":0,\"canonical_ids\":0,\"results\":[{\"message_id\":\"0:1574768457994369%651d26c5f9fd7ecd\"}]}', '2019-11-26 11:40:58'),
(31, 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', '{\"id\":\"50\",\"message_type\":\"text\",\"message_body\":\"gugyfyfu\",\"message_from\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"message_to\":\"WOwg1gJcN6ZeEOwuV7ZSqNVg4s92\",\"group_id\":\"1\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 17:10:59\",\"modified_date\":\"2019-11-26 17:10:59\",\"sendername\":\"Tanmay\"}', '{\"multicast_id\":4756664679019061056,\"success\":1,\"failure\":0,\"canonical_ids\":0,\"results\":[{\"message_id\":\"0:1574768459123658%651d26c5f9fd7ecd\"}]}', '2019-11-26 11:40:59'),
(32, 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', '{\"id\":\"51\",\"message_type\":\"text\",\"message_body\":\"vhfyguv\",\"message_from\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"message_to\":\"WOwg1gJcN6ZeEOwuV7ZSqNVg4s92\",\"group_id\":\"1\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 17:11:00\",\"modified_date\":\"2019-11-26 17:11:00\",\"sendername\":\"Tanmay\"}', '{\"multicast_id\":1346045786232129478,\"success\":1,\"failure\":0,\"canonical_ids\":0,\"results\":[{\"message_id\":\"0:1574768460362749%651d26c5f9fd7ecd\"}]}', '2019-11-26 11:41:00'),
(33, 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', '{\"id\":\"52\",\"message_type\":\"text\",\"message_body\":\"ghgug\",\"message_from\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"message_to\":\"WOwg1gJcN6ZeEOwuV7ZSqNVg4s92\",\"group_id\":\"1\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 17:11:01\",\"modified_date\":\"2019-11-26 17:11:01\",\"sendername\":\"Tanmay\"}', '{\"multicast_id\":8447039646051210008,\"success\":1,\"failure\":0,\"canonical_ids\":0,\"results\":[{\"message_id\":\"0:1574768461277820%651d26c5f9fd7ecd\"}]}', '2019-11-26 11:41:01'),
(34, 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', '{\"id\":\"53\",\"message_type\":\"text\",\"message_body\":\"chcyfu\",\"message_from\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"message_to\":\"WOwg1gJcN6ZeEOwuV7ZSqNVg4s92\",\"group_id\":\"1\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 17:11:01\",\"modified_date\":\"2019-11-26 17:11:01\",\"sendername\":\"Tanmay\"}', '{\"multicast_id\":6587484147180011836,\"success\":1,\"failure\":0,\"canonical_ids\":0,\"results\":[{\"message_id\":\"0:1574768462154746%651d26c5f9fd7ecd\"}]}', '2019-11-26 11:41:02'),
(35, 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', '{\"id\":\"54\",\"message_type\":\"text\",\"message_body\":\"cygig\",\"message_from\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"message_to\":\"WOwg1gJcN6ZeEOwuV7ZSqNVg4s92\",\"group_id\":\"1\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 17:11:02\",\"modified_date\":\"2019-11-26 17:11:02\",\"sendername\":\"Tanmay\"}', '{\"multicast_id\":3355862386639414256,\"success\":1,\"failure\":0,\"canonical_ids\":0,\"results\":[{\"message_id\":\"0:1574768463001033%651d26c5f9fd7ecd\"}]}', '2019-11-26 11:41:03'),
(36, 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', '{\"id\":\"55\",\"message_type\":\"text\",\"message_body\":\"chchftducgc\",\"message_from\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"message_to\":\"WOwg1gJcN6ZeEOwuV7ZSqNVg4s92\",\"group_id\":\"1\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 17:11:14\",\"modified_date\":\"2019-11-26 17:11:14\",\"sendername\":\"Tanmay\"}', '{\"multicast_id\":4514531786059788365,\"success\":1,\"failure\":0,\"canonical_ids\":0,\"results\":[{\"message_id\":\"0:1574768474635654%651d26c5f9fd7ecd\"}]}', '2019-11-26 11:41:14'),
(37, 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'QdF47o6FVmapiSPeLonFxJWxboN2', '{\"id\":\"56\",\"message_type\":\"text\",\"message_body\":\"fyfyftdyfyfhxcgxyctdt\",\"message_from\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"message_to\":\"QdF47o6FVmapiSPeLonFxJWxboN2\",\"group_id\":\"2\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 17:11:20\",\"modified_date\":\"2019-11-26 17:11:20\",\"sendername\":\"Tanmay\"}', '{\"multicast_id\":124834857707029400,\"success\":1,\"failure\":0,\"canonical_ids\":0,\"results\":[{\"message_id\":\"0:1574768480947509%651d26c5f9fd7ecd\"}]}', '2019-11-26 11:41:20'),
(38, 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'QdF47o6FVmapiSPeLonFxJWxboN2', '{\"id\":\"57\",\"message_type\":\"text\",\"message_body\":\"vhfyfufu\",\"message_from\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"message_to\":\"QdF47o6FVmapiSPeLonFxJWxboN2\",\"group_id\":\"2\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 17:11:22\",\"modified_date\":\"2019-11-26 17:11:22\",\"sendername\":\"Tanmay\"}', '{\"multicast_id\":3690676405115229004,\"success\":1,\"failure\":0,\"canonical_ids\":0,\"results\":[{\"message_id\":\"0:1574768482412936%651d26c5f9fd7ecd\"}]}', '2019-11-26 11:41:22'),
(39, 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'QdF47o6FVmapiSPeLonFxJWxboN2', '{\"id\":\"58\",\"message_type\":\"text\",\"message_body\":\"higufyf\",\"message_from\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"message_to\":\"QdF47o6FVmapiSPeLonFxJWxboN2\",\"group_id\":\"2\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 17:11:23\",\"modified_date\":\"2019-11-26 17:11:23\",\"sendername\":\"Tanmay\"}', '{\"multicast_id\":4489966955197001129,\"success\":1,\"failure\":0,\"canonical_ids\":0,\"results\":[{\"message_id\":\"0:1574768483426920%651d26c5f9fd7ecd\"}]}', '2019-11-26 11:41:23'),
(40, 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'QdF47o6FVmapiSPeLonFxJWxboN2', '{\"id\":\"59\",\"message_type\":\"text\",\"message_body\":\"gjgug\",\"message_from\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"message_to\":\"QdF47o6FVmapiSPeLonFxJWxboN2\",\"group_id\":\"2\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 17:11:24\",\"modified_date\":\"2019-11-26 17:11:24\",\"sendername\":\"Tanmay\"}', '{\"multicast_id\":5501114943307645911,\"success\":1,\"failure\":0,\"canonical_ids\":0,\"results\":[{\"message_id\":\"0:1574768484433635%651d26c5f9fd7ecd\"}]}', '2019-11-26 11:41:24'),
(41, 'QdF47o6FVmapiSPeLonFxJWxboN2', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', '{\"id\":\"60\",\"message_type\":\"text\",\"message_body\":\" Gchfh\",\"message_from\":\"QdF47o6FVmapiSPeLonFxJWxboN2\",\"message_to\":\"WOwg1gJcN6ZeEOwuV7ZSqNVg4s92\",\"group_id\":\"3\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 18:31:00\",\"modified_date\":\"2019-11-26 18:31:00\",\"sendername\":\"Nayan2\"}', '{\"multicast_id\":7258390926274734359,\"success\":1,\"failure\":0,\"canonical_ids\":0,\"results\":[{\"message_id\":\"0:1574773260699652%651d26c5f9fd7ecd\"}]}', '2019-11-26 13:01:00'),
(42, 'QdF47o6FVmapiSPeLonFxJWxboN2', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', '{\"id\":\"61\",\"message_type\":\"text\",\"message_body\":\"Ysusgsh\",\"message_from\":\"QdF47o6FVmapiSPeLonFxJWxboN2\",\"message_to\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"group_id\":\"2\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 18:38:02\",\"modified_date\":\"2019-11-26 18:38:02\",\"sendername\":\"Nayan2\"}', '{\"multicast_id\":6314602783992951676,\"success\":1,\"failure\":0,\"canonical_ids\":0,\"results\":[{\"message_id\":\"0:1574773682888734%651d26c5f9fd7ecd\"}]}', '2019-11-26 13:08:02'),
(43, 'QdF47o6FVmapiSPeLonFxJWxboN2', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', '{\"id\":\"62\",\"message_type\":\"text\",\"message_body\":\"Hshshd\",\"message_from\":\"QdF47o6FVmapiSPeLonFxJWxboN2\",\"message_to\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"group_id\":\"2\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 18:38:03\",\"modified_date\":\"2019-11-26 18:38:03\",\"sendername\":\"Nayan2\"}', '{\"multicast_id\":185408824710828498,\"success\":1,\"failure\":0,\"canonical_ids\":0,\"results\":[{\"message_id\":\"0:1574773683906377%651d26c5f9fd7ecd\"}]}', '2019-11-26 13:08:03'),
(44, 'QdF47o6FVmapiSPeLonFxJWxboN2', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', '{\"id\":\"63\",\"message_type\":\"text\",\"message_body\":\"Vdhdjd\",\"message_from\":\"QdF47o6FVmapiSPeLonFxJWxboN2\",\"message_to\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"group_id\":\"2\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 18:38:04\",\"modified_date\":\"2019-11-26 18:38:04\",\"sendername\":\"Nayan2\"}', '{\"multicast_id\":6848780355031735317,\"success\":1,\"failure\":0,\"canonical_ids\":0,\"results\":[{\"message_id\":\"0:1574773685001413%651d26c5f9fd7ecd\"}]}', '2019-11-26 13:08:05'),
(45, 'QdF47o6FVmapiSPeLonFxJWxboN2', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', '{\"id\":\"64\",\"message_type\":\"text\",\"message_body\":\"Dbdhdh\",\"message_from\":\"QdF47o6FVmapiSPeLonFxJWxboN2\",\"message_to\":\"iyNYZXnS68R8Fn1UGEAUSvRqZ532\",\"group_id\":\"2\",\"seen\":\"0\",\"is_first\":\"0\",\"multimedia_path\":\"\",\"is_downloaded\":\"0\",\"is_deleted\":\"0\",\"is_scheduled\":\"0\",\"Schedule_datetime\":\"0000-00-00 00:00:00\",\"created_date\":\"2019-11-26 18:38:05\",\"modified_date\":\"2019-11-26 18:38:05\",\"sendername\":\"Nayan2\"}', '{\"multicast_id\":4128914920760926672,\"success\":1,\"failure\":0,\"canonical_ids\":0,\"results\":[{\"message_id\":\"0:1574773686128774%651d26c5f9fd7ecd\"}]}', '2019-11-26 13:08:06');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, 93),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, 355),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, 213),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, 1684),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, 376),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, 244),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, 1264),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, 0),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, 1268),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, 54),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, 374),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, 297),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, 61),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, 43),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, 994),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, 1242),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, 973),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, 880),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, 1246),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, 375),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, 32),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, 501),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, 229),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, 1441),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, 975),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, 591),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, 387),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, 267),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, 0),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, 55),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, 246),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, 673),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, 359),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, 226),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, 257),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, 855),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, 237),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, 1),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, 238),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, 1345),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, 236),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, 235),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, 56),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, 86),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, 61),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, 672),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, 57),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, 269),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, 242),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, 242),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, 682),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, 506),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, 225),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, 385),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, 53),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, 357),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, 420),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, 45),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, 253),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, 1767),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, 1809),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, 593),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, 20),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, 503),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, 240),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, 291),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, 372),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, 251),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, 500),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, 298),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, 679),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, 358),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, 33),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, 594),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, 689),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, 0),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, 241),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, 220),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, 995),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, 49),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, 233),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, 350),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, 30),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, 299),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, 1473),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, 590),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, 1671),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, 502),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, 224),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, 245),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, 592),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, 509),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, 0),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, 39),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, 504),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, 852),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, 36),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, 354),
(99, 'IN', 'INDIA', 'India', 'IND', 356, 91),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, 62),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, 98),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, 964),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, 353),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, 972),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, 39),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, 1876),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, 81),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, 962),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, 7),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, 254),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, 686),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, 850),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, 82),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, 965),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, 996),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, 856),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, 371),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, 961),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, 266),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, 231),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, 218),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, 423),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, 370),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, 352),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, 853),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, 389),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, 261),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, 265),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, 60),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, 960),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, 223),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, 356),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, 692),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, 596),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, 222),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, 230),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, 269),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, 52),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, 691),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, 373),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, 377),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, 976),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, 1664),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, 212),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, 258),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, 95),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, 264),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, 674),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, 977),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, 31),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, 599),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, 687),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, 64),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, 505),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, 227),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, 234),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, 683),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, 672),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, 1670),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, 47),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, 968),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, 92),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, 680),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, 970),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, 507),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, 675),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, 595),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, 51),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, 63),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, 0),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, 48),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, 351),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, 1787),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, 974),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, 262),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, 40),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, 70),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, 250),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, 290),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, 1869),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, 1758),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, 508),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, 1784),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, 684),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, 378),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, 239),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, 966),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, 221),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, 381),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, 248),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, 232),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, 65),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, 421),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, 386),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, 677),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, 252),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, 27),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, 0),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, 34),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, 94),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, 249),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, 597),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, 47),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, 268),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, 46),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, 41),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, 963),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, 886),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, 992),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, 255),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, 66),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, 670),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, 228),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, 690),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, 676),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, 1868),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, 216),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, 90),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, 7370),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, 1649),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, 688),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, 256),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, 380),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, 971),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, 44),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, 1),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, 1),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, 598),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, 998),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, 678),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, 58),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, 84),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, 1284),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, 1340),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, 681),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, 212),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, 967),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, 260),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, 263);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `message_type` varchar(255) NOT NULL,
  `message_body` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message_from` text NOT NULL,
  `message_to` text NOT NULL,
  `group_id` int(11) NOT NULL,
  `seen` enum('0','1') NOT NULL DEFAULT '0',
  `is_first` enum('0','1') NOT NULL,
  `multimedia_path` varchar(255) NOT NULL,
  `is_downloaded` enum('0','1') NOT NULL,
  `is_deleted` enum('0','1') NOT NULL,
  `is_scheduled` enum('0','1') NOT NULL,
  `Schedule_datetime` datetime NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `modified_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `message_type`, `message_body`, `message_from`, `message_to`, `group_id`, `seen`, `is_first`, `multimedia_path`, `is_downloaded`, `is_deleted`, `is_scheduled`, `Schedule_datetime`, `created_date`, `modified_date`) VALUES
(1, 'text', 'hi', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '1', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-25 18:35:10', '2019-11-25 18:35:10'),
(2, 'text', 'xhdhfhchch', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-25 18:35:17', '2019-11-25 18:35:17'),
(3, 'text', ' bcbfhfhf', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-25 18:35:17', '2019-11-25 18:35:17'),
(4, 'text', 'tuioyh', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-25 19:19:29', '2019-11-25 19:19:29'),
(5, 'text', '😁', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-25 22:51:04', '2019-11-25 22:51:04'),
(6, 'text', '🤑', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 11:33:10', '2019-11-26 11:33:10'),
(7, 'text', 'notification', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 12:48:28', '2019-11-26 12:48:28'),
(8, 'text', 'postman', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 12:58:43', '2019-11-26 12:58:43'),
(9, 'text', 'postman', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 13:03:06', '2019-11-26 13:03:06'),
(10, 'text', 'postman', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 13:10:51', '2019-11-26 13:10:51'),
(11, 'text', 'postman', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 13:11:31', '2019-11-26 13:11:31'),
(12, 'text', 'postman', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 13:12:35', '2019-11-26 13:12:35'),
(13, 'text', 'postman', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 13:13:13', '2019-11-26 13:13:13'),
(14, 'text', 'postman', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 13:14:40', '2019-11-26 13:14:40'),
(15, 'text', 'postman', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 13:15:22', '2019-11-26 13:15:22'),
(16, 'text', 'Tanmay ', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 13:16:17', '2019-11-26 13:16:17'),
(17, 'text', 'hello  kunal', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 13:17:05', '2019-11-26 13:17:05'),
(18, 'text', 'fufy', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 13:17:28', '2019-11-26 13:17:28'),
(19, 'text', 'postman tanmay', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 13:52:51', '2019-11-26 13:52:51'),
(20, 'text', 'postman tanmay', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 13:55:27', '2019-11-26 13:55:27'),
(21, 'text', 'postman tanmay', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 13:57:41', '2019-11-26 13:57:41'),
(22, 'text', 'postman tanmay', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 14:01:45', '2019-11-26 14:01:45'),
(23, 'text', 'yes', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 14:03:54', '2019-11-26 14:03:54'),
(24, 'text', 'postman tanmay', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 14:20:26', '2019-11-26 14:20:26'),
(25, 'text', 'postman tanmay', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 14:22:51', '2019-11-26 14:22:51'),
(26, 'text', 'postman tanmay', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 14:23:24', '2019-11-26 14:23:24'),
(27, 'text', 'postman tanmay', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 14:28:20', '2019-11-26 14:28:20'),
(28, 'text', 'postman tanmay', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 14:29:27', '2019-11-26 14:29:27'),
(29, 'text', 'postman tanmay', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 14:30:02', '2019-11-26 14:30:02'),
(30, 'text', 'postman tanmay', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 14:30:53', '2019-11-26 14:30:53'),
(31, 'text', 'postman tanmay', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 14:32:27', '2019-11-26 14:32:27'),
(32, 'text', 'postman tanmay', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 14:33:36', '2019-11-26 14:33:36'),
(33, 'text', 'postman tanmay', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 14:34:07', '2019-11-26 14:34:07'),
(34, 'text', 'postman tanmay', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 14:38:18', '2019-11-26 14:38:18'),
(35, 'text', 'postman tanmay', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 14:41:46', '2019-11-26 14:41:46'),
(36, 'text', 'postman tanmay', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 14:47:22', '2019-11-26 14:47:22'),
(37, 'text', 'hi nayan', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'QdF47o6FVmapiSPeLonFxJWxboN2', 2, '0', '1', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 15:06:13', '2019-11-26 15:06:13'),
(38, 'text', 'Vhgh', 'QdF47o6FVmapiSPeLonFxJWxboN2', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 2, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 15:07:32', '2019-11-26 15:07:32'),
(39, 'text', 'Chvhvugy', 'QdF47o6FVmapiSPeLonFxJWxboN2', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 2, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 15:08:13', '2019-11-26 15:08:13'),
(40, 'text', 'xgdgd', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'QdF47o6FVmapiSPeLonFxJWxboN2', 2, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 15:21:18', '2019-11-26 15:21:18'),
(41, 'text', 'Gufhfhfu', 'QdF47o6FVmapiSPeLonFxJWxboN2', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 2, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 15:21:33', '2019-11-26 15:21:33'),
(42, 'text', 'fhfufu', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'QdF47o6FVmapiSPeLonFxJWxboN2', 2, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 16:58:07', '2019-11-26 16:58:07'),
(43, 'text', 'hello test1', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 17:08:48', '2019-11-26 17:08:48'),
(44, 'text', 'ok notification', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 17:09:54', '2019-11-26 17:09:54'),
(45, 'text', 'hello test1', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 'QdF47o6FVmapiSPeLonFxJWxboN2', 3, '0', '1', '', '0', '1', '0', '0000-00-00 00:00:00', '2019-11-26 17:10:11', '2019-11-26 17:34:34'),
(46, 'text', 'Vjcgcgcg', 'QdF47o6FVmapiSPeLonFxJWxboN2', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 3, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 17:10:41', '2019-11-26 17:10:41'),
(47, 'text', 'vhchfyfy', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 17:10:55', '2019-11-26 17:10:55'),
(48, 'text', ' hchf', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 17:10:56', '2019-11-26 17:10:56'),
(49, 'text', 'vhfufyfy', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 17:10:57', '2019-11-26 17:10:57'),
(50, 'text', 'gugyfyfu', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 17:10:59', '2019-11-26 17:10:59'),
(51, 'text', 'vhfyguv', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 17:11:00', '2019-11-26 17:11:00'),
(52, 'text', 'ghgug', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 17:11:01', '2019-11-26 17:11:01'),
(53, 'text', 'chcyfu', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 17:11:01', '2019-11-26 17:11:01'),
(54, 'text', 'cygig', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 17:11:02', '2019-11-26 17:11:02'),
(55, 'text', 'chchftducgc', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 17:11:14', '2019-11-26 17:11:14'),
(56, 'text', 'fyfyftdyfyfhxcgxyctdt', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'QdF47o6FVmapiSPeLonFxJWxboN2', 2, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 17:11:20', '2019-11-26 17:11:20'),
(57, 'text', 'vhfyfufu', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'QdF47o6FVmapiSPeLonFxJWxboN2', 2, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 17:11:22', '2019-11-26 17:11:22'),
(58, 'text', 'higufyf', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'QdF47o6FVmapiSPeLonFxJWxboN2', 2, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 17:11:23', '2019-11-26 17:11:23'),
(59, 'text', 'gjgug', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'QdF47o6FVmapiSPeLonFxJWxboN2', 2, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 17:11:24', '2019-11-26 17:11:24'),
(60, 'text', ' Gchfh', 'QdF47o6FVmapiSPeLonFxJWxboN2', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 3, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 18:31:00', '2019-11-26 18:31:00'),
(61, 'text', 'Ysusgsh', 'QdF47o6FVmapiSPeLonFxJWxboN2', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 2, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 18:38:02', '2019-11-26 18:38:02'),
(62, 'text', 'Hshshd', 'QdF47o6FVmapiSPeLonFxJWxboN2', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 2, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 18:38:03', '2019-11-26 18:38:03'),
(63, 'text', 'Vdhdjd', 'QdF47o6FVmapiSPeLonFxJWxboN2', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 2, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 18:38:04', '2019-11-26 18:38:04'),
(64, 'text', 'Dbdhdh', 'QdF47o6FVmapiSPeLonFxJWxboN2', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 2, '0', '0', '', '0', '0', '0', '0000-00-00 00:00:00', '2019-11-26 18:38:05', '2019-11-26 18:38:05');

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `id` int(11) NOT NULL,
  `msg` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `userId` text NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `country_code` int(4) NOT NULL,
  `image` text NOT NULL,
  `thumb_image` text NOT NULL,
  `device_token` text NOT NULL,
  `user_token` text NOT NULL,
  `last_seen_hide` enum('0','1') NOT NULL,
  `auto_download` enum('0','1') NOT NULL,
  `os_type` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `modified_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `userId`, `mobile`, `country_code`, `image`, `thumb_image`, `device_token`, `user_token`, `last_seen_hide`, `auto_download`, `os_type`, `created_date`, `modified_date`, `status`) VALUES
(1, 'kunal', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', '9594424591', 91, 'default', 'default', 'f38hnei480k:APA91bEH0vZzto_VOQym1FCUEAV8S740Gcbbuq_AuQ1vEhJeGqBglXX_v6LEa2t65WX7wrMq-gObxFTf2pQPWxB6EfK4vzS3EqsIQKx0QLnoU9Z7p2glKuxamWJjvxVSf5eZOM1qtAMT', '018fa4756587bf946a2631e748996ee9c21593fc86a6a487e82fe0b6cc9b', '0', '0', 'A', '2019-11-22 16:10:07', '2019-11-26 17:07:37', ''),
(2, 'keyur', '7T951WEGSvZZA0skNovYm1NYZxc2', '9870609656', 91, 'default', 'default', 'fFVMjXvhFDI:APA91bEUyo1eBQ2eOMSurz-VOga38dJ0Lkr5H6YB1pV7ScLDuWhB6cgj26kIaUbmowoxOJ1qgoLq4nx4t5IvAAGj1Dy8e_49ivpRhaLPComPBIOSoeEcC6qmUXz5jo-FS3L9HnuGRuvT', 'ce3f230f03390fdedffd934765717bed0085f08c1bc6e3b80a1d4509abfe', '0', '0', 'A', '2019-11-22 16:13:30', '2019-11-22 16:13:30', ''),
(3, 'ashu new117', 'XqKmdG3VV4XLSwpgpYcTKt0EYX02', '9594955117', 91, 'default', 'default', 'cPP1CgDyRTw:APA91bEzUtd4K9zNhAU3GG4MJ3kbSENMgBuCbMQY3wWp4OFnkY2uIvnc6ZgBbC9yKRaMRgQaqBfyqtrD-7Z852x2LA-1wEsF9npFhKH83wjNKRT3G_yMZIjCGbt9ZB4kXt3l8DCXwdX4', '7f269ff68ff1782c623a4b80813e16ebbacac451fd28611394e23fc7b7a4', '0', '0', 'A', '2019-11-23 14:39:44', '2019-11-23 14:39:44', ''),
(4, 'Majid Khan', 'z4tB1Re506aro1QVf9dlu4oSi5p1', '8080303157', 91, 'default', 'default', 'dCTFhRU5xts:APA91bEofKLAem9KTX6wxcXp2m0v6Ia5Qk88Wk6-uKD1QQvb6SuwPVwMko62z-JbQxlmuxd1AUInaI5z8MC5nRbvw8m2RXRLph7Sy7ZxLIMpSs-UxnXkp0BBjsrqK-6M5WjWg-NvIDyi', '70cafd67f977c5479012a6f061d863dc7655feab457af53de751e8682951', '0', '0', 'A', '2019-11-23 16:34:15', '2019-11-23 16:34:15', ''),
(5, 'Nayan2', 'QdF47o6FVmapiSPeLonFxJWxboN2', '7208896582', 91, 'http://13.127.177.25/chatapp/assets/upload/image/e73678a391909ffd80fe5a3f93f484661574763293.jpg', '0', 'eC1h1fz2sBw:APA91bGmGppXAfTlhs0x-1YxQA7Qmrz44kKK8uNN2ZmL3_EKyIMfy3TzX7ulRCAAzEuZ0fAGHeQltS9Nn6ZtKtTvLPASURPEd_pzgDmi71uzXHDxbcYgaxYY1zLCNGY6K16O_WKGk1LF', '748b0692559a22b53679e2b3848762d2f7ec751261c079d5bd2b7f0a159c', '0', '0', 'A', '2019-11-23 17:09:36', '2019-11-26 16:56:55', ''),
(6, 'Tanmay', 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', '9619163912', 91, 'default', 'default', 'e7d6sjJWhdE:APA91bE84OthfHNoJFHEtfN1JzhJNwWpCfqFefEoF5HMiEMChsQuItE_x2FUED__5LMJI9RC_86dw1vGw5Y1V2rRRdQ6YwwFz-S2Elp9-KqmdjfN3AP4cjHGNaQCWpUy9cd5212Oe0N3', 'a1190cbf31c7d7d558d4b2cb7c82650fb33b676c7a34b7f157cbc2b5dd16', '0', '0', 'A', '2019-11-25 17:50:12', '2019-11-26 17:22:56', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_group`
--

CREATE TABLE `user_group` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `modified_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_group`
--

INSERT INTO `user_group` (`id`, `name`, `created_date`, `modified_date`) VALUES
(1, '', '2019-11-25 18:35:10', '2019-11-25 18:35:10'),
(2, '', '2019-11-26 15:06:13', '2019-11-26 15:06:13'),
(3, '', '2019-11-26 17:10:11', '2019-11-26 17:10:11');

-- --------------------------------------------------------

--
-- Table structure for table `user_group_mapping`
--

CREATE TABLE `user_group_mapping` (
  `id` int(11) NOT NULL,
  `users` text NOT NULL,
  `user_to` text NOT NULL,
  `group_id` int(11) NOT NULL,
  `is_group` enum('0','1') NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `modified_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_group_mapping`
--

INSERT INTO `user_group_mapping` (`id`, `users`, `user_to`, `group_id`, `is_group`, `status`, `created_date`, `modified_date`) VALUES
(1, 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 1, '0', '1', '2019-11-25 18:35:10', '2019-11-25 18:35:10'),
(2, 'iyNYZXnS68R8Fn1UGEAUSvRqZ532', 'QdF47o6FVmapiSPeLonFxJWxboN2', 2, '0', '1', '2019-11-26 15:06:13', '2019-11-26 15:06:13'),
(3, 'WOwg1gJcN6ZeEOwuV7ZSqNVg4s92', 'QdF47o6FVmapiSPeLonFxJWxboN2', 3, '0', '1', '2019-11-26 17:10:11', '2019-11-26 17:10:11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `android_firebase_response`
--
ALTER TABLE `android_firebase_response`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test`
--
ALTER TABLE `test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_group`
--
ALTER TABLE `user_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_group_mapping`
--
ALTER TABLE `user_group_mapping`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `android_firebase_response`
--
ALTER TABLE `android_firebase_response`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `test`
--
ALTER TABLE `test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user_group`
--
ALTER TABLE `user_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_group_mapping`
--
ALTER TABLE `user_group_mapping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
